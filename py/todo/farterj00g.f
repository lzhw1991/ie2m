#include "fintrf.h"

C This is an example of the FORTRAN code required for interfacing
C a .MEX file to MATLAB.
C
C This subroutine is the main gateway to MATLAB.  When a MEX function
C  is executed MATLAB calls the MEXFUNCTION subroutine in the corresponding
C  MEX file.  
C
C Copyright 1984-2000 The MathWorks, Inc.
C $Revision: 1.8 $
C
      SUBROUTINE MEXFUNCTION(NLHS, PLHS, NRHS, PRHS)

      IMPLICIT NONE
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
      integer PLHS(*), PRHS(*)

C-----------------------------------------------------------------------
C

      INTEGER NLHS, NRHS
C
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
      integer MXCREATEDOUBLEMATRIX, MXGETPR, MXGETPI

C-----------------------------------------------------------------------
C

      INTEGER MXGETM, MXGETN
      LOGICAL MXISCOMPLEX

C
C KEEP THE ABOVE SUBROUTINE, ARGUMENT, AND FUNCTION DECLARATIONS FOR USE
C IN ALL YOUR FORTRAN MEX FILES.
C---------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
C  Pointers pointing on the I/O

      integer PTRNS, coord, Z_R, Z_I, k, Theta, J_R, J_I, rho_R, rho_I

C-----------------------------------------------------------------------
C

      INTEGER NN, Ntheta, NJ, NS
      REAL*8 RNS
      Logical Iscplx

C
C CHECK FOR PROPER NUMBER OF ARGUMENTS
C
      IF (NRHS .NE. 6) THEN
        CALL MEXERRMSGTXT('farterj00 requires 6 input arguments')
      ELSEIF (NLHS .GT. 1) THEN
        CALL MEXERRMSGTXT('farterj00 requires 1 output argument')
      ENDIF


      Ntheta = max(MXGETM(PRHS(1)),MXGETN(PRHS(1)))
      NJ     = max(MXGETM(PRHS(2)),MXGETN(PRHS(2)))
      NN     = MXGETN(PRHS(5))

      PTRNS = MXGETPR(PRHS(4))
      call mxCopyPtrToReal8(PTRNS,RNS,1)
      NS = int(RNS)

      IF (NJ .NE. NS) THEN
       CALL MEXERRMSGTXT(
     1'Dimensions for Currents and mesh don''t agree in farterj00')
      END IF
C

C
C CREATE A MATRIX FOR RETURN ARGUMENT
C
      PLHS(1) = MXCREATEDOUBLEMATRIX(2,Ntheta,1)

      Theta  = MXGETPR(PRHS(1))
      J_R    = MXGETPR(PRHS(2))
      rho_R  = MXGETPR(PRHS(3))

C     PTRNS  = MXGETPR(PRHS(4)) ! already gotten
      coord  = MXGETPR(PRHS(5))
      k      = MXGETPR(PRHS(6))

      Z_R    = MXGETPR(PLHS(1))
      Z_I    = MXGETPI(PLHS(1))
C DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE
C       CREATED ARRAYS.  
C
      IF (MxIsComplex(PRHS(2))) THEN
      Iscplx = .true.
      J_I    = MXGETPI(PRHS(2))
      rho_I  = MXGETPI(PRHS(3))
      CALL farterj00(%val(Z_R), %val(Z_I),  
     1           Ntheta, %val(theta), %val(J_R), %val(J_I), %val(rho_R),
     2           %val(rho_I), NS, NN, %val(coord), %val(k), Iscplx)
      ELSE
      Iscplx = .false.
      CALL farterj00(%val(Z_R), %val(Z_I), 
     1           Ntheta, %val(theta), %val(J_R), %val(J_R), %val(rho_R),
     2           %val(rho_R), NS, NN, %val(coord), %val(k), Iscplx)
      ENDIF
C


      RETURN
      END





















