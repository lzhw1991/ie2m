      SUBROUTINE snglnt1(Z_R, Z_I, NSA, NNA, coordA,
     1                              NSB, NNB, coordB,
     2                              kr, ki,Iscpl)

      IMPLICIT NONE

      INTEGER NNA, NNB
      REAL*8 Z_R(NNA,NNB), Z_I(NNA,NNB)
      REAL*8 coordA(2,NNA), coordB(2,NNB)
      REAL*8 NSA, NSB ! Stockage MATLAB des entiers
      REAL*8 kr, ki
      LOGICAL Iscpl

      CALL snglnt1_F90(Z_R, Z_I, NSA, NNA, coordA,
     1                          NSB, NNB, coordB,
     2                           kr, ki,  Iscpl)

      RETURN
      END










