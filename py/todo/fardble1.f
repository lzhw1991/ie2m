      SUBROUTINE fardble1(Z_R, Z_I, Ntheta, theta, J_R, J_I, 
     1                              NS, NN, coord, k, Iscplx)

      IMPLICIT NONE

      INTEGER NN, Ntheta
      REAL*8 Z_R(Ntheta), Z_I(Ntheta)
      REAL*8 theta(Ntheta)
      REAL*8 J_R(NN), J_I(NN)
      REAL*8 coord(2,NN)
      REAL*8 NS ! Stockage MATLAB des entiers
      REAL*8 k
      LOGICAL Iscplx

      CALL fardble1_F90(Z_R, Z_I, Ntheta, theta, J_R, J_I, 
     1                              NS, NN, coord, k, Iscplx)             

      RETURN
      END










