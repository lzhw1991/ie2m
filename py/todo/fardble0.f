      SUBROUTINE fardble0(Z_R, Z_I, Ntheta, theta, J_R, J_I, 
     1                              NS, NN, coord, k, Iscplx)

      IMPLICIT NONE
      logical Iscplx

      INTEGER NN, NS, Ntheta
      REAL*8 Z_R(Ntheta), Z_I(Ntheta)
      REAL*8 theta(Ntheta)
      REAL*8 J_R(NS), J_I(NS)
      REAL*8 coord(2,NN)
      REAL*8 k

      CALL fardble0_F90(Z_R, Z_I, Ntheta, theta, J_R, J_I, 
     1                              NS, NN, coord, k, Iscplx)             

      RETURN
      END










