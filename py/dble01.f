      SUBROUTINE dble01(Z, NSA, NNA, coordA,
     1                              NSB, NNB, coordB,
     2                              kr, ki, Iscpl)

      IMPLICIT NONE

      INTEGER NNA, NNB, NSA, NSB
      REAL*8 Z_R(NSA,NNB), Z_I(NSA,NNB)
      REAL*8 coordA(2,NNA), coordB(2,NNB)
      COMPLEX*16 kr
      REAL*8 ki
      LOGICAL Iscpl

      INTEGER ii, jj
      COMPLEX*16 Z(NSA,NNB)

Cf2py intent(in) NSA, NSB, kr
Cf2py intent(in) coordA, coordB
Cf2py intent(out) Z
Cf2py integer intent(hide),depend(coordA) :: NNA=shape(coordA,1)
Cf2py integer intent(hide),depend(coordB) :: NNB=shape(coordB,1)
Cf2py real intent(hide) :: ki
Cf2py logical intent(hide) :: Iscpl

Cf2py external dble01_F90

      IF (imag(kr) .NE. 0.0) THEN
         ki = imag(kr)
         Iscpl = .true.
      ELSE
         ki = 0.0
         Iscpl = .false.
      END IF

      CALL dble01_F90(Z_R, Z_I, NSA, NNA, coordA,
     1                              NSB, NNB, coordB,
     2                              kr, ki, Iscpl)

      do ii=1,NSA
         do jj=1,NNB
            Z(ii,jj) =  (1.0,0.0)* Z_R(ii,jj) + (0.0,1.0)* Z_I(ii,jj)
         enddo
      enddo

      RETURN
      END










