      SUBROUTINE plane0(Z, theta, NS, NN, coord, k)

      IMPLICIT NONE

      INTEGER NN, NS, ii
      COMPLEX*16 Z(NS)
      REAL*8 Z_R(NS), Z_I(NS)
      REAL*8 coord(2,NN)
      REAL*8 theta
      REAL*8 k

Cf2py intent(in) theta, NS, k
Cf2py intent(in) coord
Cf2py intent(out) Z
Cf2py integer intent(hide),depend(coord) :: NN=shape(coord,1)

Cf2py external plane0_F90

      CALL plane0_F90(Z_R, Z_I, theta, NS, NN, coord, k)

      do ii=1,NS
            Z(ii) =  (1.0,0.0)* Z_R(ii) + (0.0,1.0)* Z_I(ii)
      enddo


      RETURN
      END










