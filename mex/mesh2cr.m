function [cr, Np, ptcr] = mesh2cr(p,e,curve,open)
% mesh2cr gives a boundary curve from a 2D mesh
%
%    [cr,Np,ptcr] = mesh2cr(p,e,curve,open) returns an ie2m mesh of 
%         a curve bounding the domain itself or one of its 
%         subdomain
%
%    Input data
%    ----------    
%    p     : list of points of a pdetool mesh
%    e     : edge matrix of a pdetool mesh
%    curve : integer vectors characterizing the numbers of the boundary 
%            parts that constitute the boundary curve (located at
%            the fifth row of the edge matrix e, ... see initmesh)
%            abs(curve(1)) .. abs(curve(N)) give the numbers of the 
%            successive parts of the curve ; curve(j) > 0 if the
%            orientation of this part is compatible with that one 
%            of the curve and curve(j) is < 0 otherwise
%    open  : equal to 1 if the curve is open and 0 otherwise
%
%    Output data                
%    cr    : mesh of a closed curve in the ie2m library convention
%    ptcr  : m = ptcr(j) <-> p(:,m) = cr(:,j) 
%    Np    : number of segments

Nseg  = length(curve);       % Number of parts 

Np    = 0;
for l = 1:Nseg,
  % find the edges that constitute part l
  Edge     = find(e(5,:) == abs(curve(l)));
  ledge    = length(Edge);
  [Et,Ie]  = sort(e(3,Edge));
  Ie       = Edge(Ie);
  if curve(l) > 0, 
    cr(:,(Np+1):(Np+ledge)) = p(:,e(1,Ie));
    ptcr((Np+1):(Np+ledge)) = e(1,Ie);
    Iend = e(2,Ie(ledge));
  else
    cr(:,(Np+1):(Np+ledge)) = p(:,e(2,Ie(ledge:-1:1)));
    ptcr((Np+1):(Np+ledge)) = e(2,Ie(ledge:-1:1));
    Iend = e(1,Ie(1));
  end
  Np = Np + ledge;
end

if open,
    cr   = [cr, p(:,Iend)];
    ptcr = [ptcr, Iend];
end
