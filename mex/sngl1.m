% sngl1 Assembles single-layer contributions
%  
%   Z = sngl1(NSA,coordA,NSB,coordB,k)
%   trial functions are continuous on curve B and linear-affine
%   on each segment
%   testing functions on curve A are of the same type as on 
%   curve B
%   
%   CURVE PARAMETERS.
%      see sngl0
%
%   INPUT PARAMETERS.
%      same as in sngl0
%
%   OUTPUT PARAMETERS.
%      Z - NNA by NNB complex matrix giving the single-layer
%          interactions (NNA and NNB are respectively the number 
%          of vertices on curve A and curve B)
%        
%   IMPORTANT CAUTION.
%      The user has to manage by himself the treatment of the 
%      d.o.fs which are set to zero. This is specially the 
%      case for a problem involving an open curve. 
