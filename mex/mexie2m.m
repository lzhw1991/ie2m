function mexie2m()
fid = fopen('fortfich.txt');
X = textscan(fid, '%s');
X = deal(X{:});
Nf = length(X);
for nb = 1:Nf,
    sg = X{nb};
    n  = findstr(sg,'g.f');
    if length(n) ~=0, 
        s = [sg(1:(n-1)),'.F'];
        sg = [sg(1:(n)),'.F'];
        eval(['mex -v -I/iem2lib',' ',s,' ',sg,' ','ie2mlib/libie2mv2.dylib']);
    end
end
fclose(fid);
