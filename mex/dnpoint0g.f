#include "fintrf.h"

C This is an example of the FORTRAN code required for interfacing
C a .MEX file to MATLAB.
C
C This subroutine is the main gateway to MATLAB.  When a MEX function
C  is executed MATLAB calls the MEXFUNCTION subroutine in the corresponding
C  MEX file.  
C
C Copyright 1984-2000 The MathWorks, Inc.
C $Revision: 1.8 $
C
      SUBROUTINE MEXFUNCTION(NLHS, PLHS, NRHS, PRHS)

      IMPLICIT NONE
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
      integer PLHS(*), PRHS(*)

C-----------------------------------------------------------------------
C

      INTEGER NLHS, NRHS
C
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
      integer MXCREATEDOUBLEMATRIX, MXGETPR, MXGETPI

C-----------------------------------------------------------------------
C

      INTEGER MXGETM, MXGETN

C
C KEEP THE ABOVE SUBROUTINE, ARGUMENT, AND FUNCTION DECLARATIONS FOR USE
C IN ALL YOUR FORTRAN MEX FILES.
C---------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
C  Pointers pointing on the I/O

      integer X, PTRS, coord, k, Z_R, Z_I

C-----------------------------------------------------------------------
C

      INTEGER NN, NS
      REAL*8 RNS

C
C CHECK FOR PROPER NUMBER OF ARGUMENTS
C
      IF (NRHS .NE. 4) THEN
        CALL MEXERRMSGTXT('dnpoint0 requires 4 input arguments')
      ELSEIF (NLHS .GT. 1) THEN
        CALL MEXERRMSGTXT('dnpoint0 requires 1 output argument')
      ENDIF


      NN = MXGETN(PRHS(3))

C
C CREATE A MATRIX FOR RETURN ARGUMENT
C

      X      = MXGETPR(PRHS(1))
      PTRS   = MXGETPR(PRHS(2))
      coord  = MXGETPR(PRHS(3))
      k      = MXGETPR(PRHS(4))

      call mxCopyPtrToReal8(PTRS,RNS,1)
      NS = int(RNS)
      PLHS(1) = MXCREATEDOUBLEMATRIX(NS,1,1)
      Z_R    = MXGETPR(PLHS(1))
      Z_I    = MXGETPI(PLHS(1))


C DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE
C       CREATED ARRAYS.  
C
      CALL dnpoint0(%val(Z_R), %val(Z_I), 
     1                 %val(X), 
     2                 NS, NN, %val(coord), %val(k))
C

      RETURN
      END











