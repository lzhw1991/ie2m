% fardble1 Compute the far field pattern realted to a
%          double-layer potential 
%         
%   Z = fardble1(theta,J,NS,coord,k)
%   The density is continuous on the input curve
%   and linear-affine on each segment
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   DIRECTION(S) OF THE FAR FIELD PARAMETERS.
%      theta  -> Ntheta by 1 vector giving the angle between 
%                the x-axis and the direction in which is 
%                computed the far field pattern
% 
%   DENSITY PARAMETERS.
%      J -> node values of the density
%
%   INPUT PARAMETERS.
%      theta     - Ntheta by 1 vector (angles in radians)
%      J         - NN by 1 complex vector
%      Ns, coord - curve
%      k         - wavenumber (must be real)
%
%   OUTPUT PARAMETERS
%      Z - Netha by 1 vector
%
%   IMPORTANT CAUTION.
%      The user has possibly to complete the d.o.fs of the
%      density being equal to zero when the length of J is 
%      less than the number of nodes  

