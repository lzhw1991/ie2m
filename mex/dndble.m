% dndble Assembles normal trace of a double-layer contributions
%  
%   Z = dndble(NSA,coordA,NSB,coordB,k)
%   trial functions are continuous on curve B and linear-affine
%   on each segment
%   testing functions on curve A are of the same type as on 
%   curve B
%   
%   CURVE PARAMETERS.
%      see sngl0
%
%   INPUT PARAMETERS.
%      same as in sngl0
%
%   OUTPUT PARAMETERS.
%      Z - NNA by NNB complex matrix giving the normal trace of a
%          double-layer interactions (NNA and NNB are respectively 
%          the number of vertices on curve A and curve B)
%        
%   IMPORTANT CAUTION.
%      see that in sngl1 

