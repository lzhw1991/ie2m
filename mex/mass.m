% mass Assembles mass contributions obtained through
%      a lumping process
%  
%   trial functions are continuous on the input curve and 
%   linear-affine on each segment
%   same form for testing functions 
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   INPUT PARAMETERS.
%      NS, coord - input curve
%
%   OUTPUT PARAMETERS
%      Z - NN by NN matrix 
%          
%   IMPORTANT CAUTION.
%      see sngl1


