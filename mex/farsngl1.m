% farsngl1 Compute the far field pattern related to a
%          single-layer potential 
%         
%   Z = farsngl1(theta,J,NS,coord,k)
%   The density is continuous on the input curve
%   and linear-affine on each segment
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   DIRECTION(S) OF THE FAR FIELD PARAMETERS.
%      see fardbl1
% 
%   DENSITY PARAMETERS.
%      see fardbl1
%
%   INPUT PARAMETERS.
%      see fardbl1
%
%   OUTPUT PARAMETERS
%      Z - Netha by 1 vector
%
%   IMPORTANT CAUTION.
%      The user has possibly to complete the d.o.fs of the
%      density being equal to zero when the length of J 
%      is less than the number of nodes  

