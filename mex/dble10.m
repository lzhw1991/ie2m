% dble10 Assembles double-layer contributions
%  
%   Z = dble10(NSA,coordA,NSB,coordB,k)
%   trial functions on curve B are constant on each segment
%   testing functions are continuous on curve A and linear-affine
%   on each segment
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   INPUT PARAMETERS.
%      see sngl0
%
%   OUTPUT PARAMETERS
%      Z - NSA by NNB complex matrix giving the double-layer
%          interactions   
%
%   IMPORTANT CAUTION.
%      see sngl1
