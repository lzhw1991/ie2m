%  IE2M Toolbox
%  Integral Equations for 2D Electromagnetism Toolbox
%  Version 2.00 -- 14-Jan-2008 
%  Author: A. Bendali -- all rights reserved
%
%  Please report bugs and errors to
%  abendali@insa-toulouse.fr 
%
%  IE2M algorithms.
%
%  Matrices associated to potentials
%   sngl0   - assembles single-layer (P0 testing and P0 trial)
%   sngl1   - assembles single-layer (P1 testing and P1 trial)
%   dndble  - assembles normal trace of double-layer
%   dble0   - assembles double-layer (P0 testing and P0 trial)
%   dble1   - assembles double-layer (P1 testing and P1 trial)
%   dble01  - assembles double-layer (P0 testing and P1 trial)
%   dble10  - assembles double-layer (P1 testing and P0 trial)
%
%  Mass matrices
%   mass     - assembles lumped mass matrix
%   mass0    - assembles mass matrix (P0 testing and trial)
%   mass1    - assembles mass matrix (P1 testing and trial)
%
%  RHS matrices for a plane wave incident field
%   plane0   - assembles the contributions of the opposite of
%              scalar incident wave (P0-discontinuous test functions)
%   plane1   - assembles the contributions of the opposite of
%              scalar incident wave (P1-continuous test functions)  
%   dnpl0    - same as plane0 but with the normal derivative
%   dnpl1    - same as plane1 but with the normal derivative 
%
%  Far field patterns of potentials
%   farsngl0 - far field patterns created by a single-layer 
%              potential (P0-by-element density)
%   farsngl1 - far field patterns created by a single-layer 
%             potential (P1-by-element density)
%   fardble0 - far field patterns created by a double-layer 
%              potential (P0-by-element density)
%   fardble1 - far field patterns created by a double-layer 
%              potential (P1-by-element density) 
