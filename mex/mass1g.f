#include "fintrf.h"

C This is an example of the FORTRAN code required for interfacing
C a .MEX file to MATLAB.
C
C This subroutine is the main gateway to MATLAB.  When a MEX function
C  is executed MATLAB calls the MEXFUNCTION subroutine in the corresponding
C  MEX file.  
C
C Copyright 1984-2000 The MathWorks, Inc.
C $Revision: 1.8 $
C
      SUBROUTINE MEXFUNCTION(NLHS, PLHS, NRHS, PRHS)

      IMPLICIT NONE
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
      integer PLHS(*), PRHS(*)

C-----------------------------------------------------------------------
C

      INTEGER NLHS, NRHS
C
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
      integer MXCREATEDOUBLEMATRIX, MXGETPR, MXGETPI

C-----------------------------------------------------------------------
C

      INTEGER MXGETM, MXGETN

C
C KEEP THE ABOVE SUBROUTINE, ARGUMENT, AND FUNCTION DECLARATIONS FOR USE
C IN ALL YOUR FORTRAN MEX FILES.
C---------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
C  Pointers pointing on the I/O

      integer NS, coord, Z

C-----------------------------------------------------------------------
C

      INTEGER NN

C
C CHECK FOR PROPER NUMBER OF ARGUMENTS
C
      IF (NRHS .NE. 2) THEN
        CALL MEXERRMSGTXT('mass1 requires 2 input arguments')
      ELSEIF (NLHS .GT. 1) THEN
        CALL MEXERRMSGTXT('mass1 requires 1 output argument')
      ENDIF


      NN = MXGETN(PRHS(2))
C

C
C CREATE A MATRIX FOR RETURN ARGUMENT
C
      PLHS(1) = MXCREATEDOUBLEMATRIX(NN,NN,0)

      NS     = MXGETPR(PRHS(1))
      coord  = MXGETPR(PRHS(2))

      Z      = MXGETPR(PLHS(1))

C DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE
C       CREATED ARRAYS.  
C
      CALL mass1(%val(Z), %val(NS), NN, %val(coord))  
C

      RETURN
      END











