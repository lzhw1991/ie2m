% fardble0 Compute the far field pattern realted to a
%          double-layer potential 
%         
%   Z = fardble0(theta,J,NS,coord,k)
%   The density is constant on each segment
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   DIRECTION(S) OF THE FAR FIELD PARAMETERS.
%      see fardble1
% 
%   DENSITY PARAMETERS.
%      J -> values of the density on each segment
%
%   INPUT PARAMETERS.
%      theta     - Ntheta by 1 vector (angles in radians)
%      J         - NS by 1 complex vector
%      Ns, coord - curve
%      k         - wavenumber (must be real)
%
%   OUTPUT PARAMETERS
%      Z - Ntetha by 1 vector
%
