#include "fintrf.h"

C This is an example of the FORTRAN code required for interfacing
C a .MEX file to MATLAB.
C
C This subroutine is the main gateway to MATLAB.  When a MEX function
C  is executed MATLAB calls the MEXFUNCTION subroutine in the corresponding
C  MEX file.  
C
C Copyright 1984-2000 The MathWorks, Inc.
C $Revision: 1.8 $
C
      SUBROUTINE MEXFUNCTION(NLHS, PLHS, NRHS, PRHS)

      IMPLICIT NONE
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
      integer PLHS(*), PRHS(*)

C-----------------------------------------------------------------------
C

      INTEGER NLHS, NRHS
C
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
      integer MXCREATEDOUBLEMATRIX, MXGETPR, MXGETPI

C-----------------------------------------------------------------------
C

      INTEGER MXGETM, MXGETN
      LOGICAL MXISCOMPLEX
C
C KEEP THE ABOVE SUBROUTINE, ARGUMENT, AND FUNCTION DECLARATIONS FOR USE
C IN ALL YOUR FORTRAN MEX FILES.
C---------------------------------------------------------------------
C
C-----------------------------------------------------------------------
C     (pointer) Replace integer by integer on the DEC Alpha
C     64-bit platform
C
C  Pointers pointing on the I/O

      integer PTRNSA, coordA, PTRNSB, coordB, Z_R, Z_I, k_r, k_i

C-----------------------------------------------------------------------
C

      INTEGER NSA, NSB, NNA, NNB
      real*8 RNSA, RNSB
      real*8 ki
      LOGICAL Iscpl
C
C CHECK FOR PROPER NUMBER OF ARGUMENTS
C
      IF (NRHS .NE. 5) THEN
        CALL MEXERRMSGTXT('drsngl0 requires 5 input arguments')
      ELSEIF (NLHS .GT. 1) THEN
        CALL MEXERRMSGTXT('drsngl0 requires 1 output argument')
      ENDIF


      NNA = MXGETN(PRHS(2))
      NNB = MXGETN(PRHS(4))


C
C CREATE A MATRIX FOR RETURN ARGUMENT
C

      PTRNSA = MXGETPR(PRHS(1))
      Call MXCOPYPTRTOREAL8(PTRNSA, RNSA, 1) ! Stockage Matlab entiers
      NSA    = int(RNSA)   ! Transtypage
      coordA = MXGETPR(PRHS(2))
      PTRNSB = MXGETPR(PRHS(3))
      Call MXCOPYPTRTOREAL8(PTRNSB, RNSB, 1) ! Stockage Matlab entiers
      NSB    = int(RNSB)   ! Transtypage

      PLHS(1) = MXCREATEDOUBLEMATRIX(NSA,NSB,1)

      coordB = MXGETPR(PRHS(4))
      k_r    = MXGETPR(PRHS(5))
      IF (MxIsComplex(PRHS(5))) THEN
         Iscpl = .true.
         k_i = MXGETPI(PRHS(5))
         Call MXCOPYPTRTOREAL8(k_i, ki, 1)
      ELSE
         ki = 0.
         Iscpl = .false.
      END IF

      Z_R    = MXGETPR(PLHS(1))
      Z_I    = MXGETPI(PLHS(1))
C DO THE ACTUAL COMPUTATIONS IN A SUBROUTINE
C       CREATED ARRAYS.  

C
      CALL drsngl0(%val(Z_R), %val(Z_I), 
     1        NSA, NNA, %val(coordA),
     2        NSB, NNB, %val(coordB), %val(k_r), ki, Iscpl)
C

      RETURN
      END











