% dble01 Assembles double-layer contributions
%  
%   Z = dble01(NSA,coordA,NSB,coordB,k)
%   trial functions are continuous on curve B and linear-affine
%   on each segment
%   testing functions on curve A are constant on each segment
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   INPUT PARAMETERS.
%      see sngl0
%
%   OUTPUT PARAMETERS
%      Z - NSA by NNB complex matrix giving the double-layer
%          interactions   
%
%   IMPORTANT CAUTION.
%      see sngl1
