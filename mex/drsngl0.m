% drsngl0 Assembles derivative relative to the wavenumber
% of the single-layer potential
%  
%   Z = drsngl0(NSA,coordA,NSB,coordB,k)
%   trial functions on curve B are constant on each segment
%   testing functions on curve A are of the same type as on 
%   curve B
%
%
%   CURVE PARAMETERS.   
%   curve   -> NS                - Number of segments
%              coord(1:2,1:NNA)  - coordinates of vertices
%              NN == NS     (curve is closed)
%              NN == NS + 1 (it is open)
%
%   INPUT PARAMETERS.
%      NSA, coordA - curve A
%      NSB, coordB - curve B
%      k           - wavenumber (may be complex)
%
%   OUTPUT PARAMETERS
%      Z - NSA by NSB complex matrix giving the single-layer
%          interactions   
