% farsngl0 Compute the far field pattern realted to a
%          single-layer potential 
%         
%   Z = farsngl0(theta,J,NS,coord,k)
%   The density is constant on each segment
%   
%   CURVE PARAMETERS. 
%      see sngl0
%
%   DIRECTION(S) OF THE FAR FIELD PARAMETERS.
%      see fardble1
% 
%   DENSITY PARAMETERS.
%      J -> values of the density on each segment
%
%   INPUT PARAMETERS.
%      see fardble0
%
%   OUTPUT PARAMETERS
%      Z - Ntetha by 1 vector
%
