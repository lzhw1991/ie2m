subroutine  dndble_F90(Z_R, Z_I, NSA, NNA, coordA,    &
     &                           NSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none

integer ::  NSA, NNA, NSB, NNB , ii, jj
!real(wp) :: RNSA, RNSB ! Stockage MATLAB de NSA et NSB
real(wp) :: Z_R(NNA,NNB), Z_I(NNA,NNB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl

!! local
complex(wp) :: kc

type(curve) :: curveA, curveB

! NSA = int(RNSA)
curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

! NSB = int(RNSB)
curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB

kc = cmplx(kr,ki)

if(Iscpl) then
   call dndble_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call dndble_base_r(curveA, curveB, kr, Z_R, Z_I)
end if
   
end subroutine dndble_F90


subroutine  sngltt1_F90(Z_R, Z_I, RNSA, NNA, coordA,    &
     &                           RNSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none

integer ::  NSA, NNA, NSB, NNB
real(wp) :: RNSA, RNSB ! Stockage MATLAB de NSA et NSB
real(wp) :: Z_R(NNA,NNB), Z_I(NNA,NNB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl

!! local
complex(wp) :: kc

type(curve) :: curveA, curveB

NSA = int(RNSA)
curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

NSB = int(RNSB)
curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB

kc = cmplx(kr,ki)

if(Iscpl) then
   call sngltt1_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call sngltt1_base_r(curveA, curveB, kr, Z_R, Z_I)
end if
   
end subroutine sngltt1_F90

subroutine  snglnt1_F90(Z_R, Z_I, RNSA, NNA, coordA,    &
     &                           RNSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none

integer ::  NSA, NNA, NSB, NNB
real(wp) :: RNSA, RNSB ! Stockage MATLAB de NSA et NSB
real(wp) :: Z_R(NNA,NNB), Z_I(NNA,NNB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl

!! local
complex(wp) :: kc

type(curve) :: curveA, curveB

NSA = int(RNSA)
curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

NSB = int(RNSB)
curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB

kc = cmplx(kr,ki)

if(Iscpl) then
   call snglnt1_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call snglnt1_base_r(curveA, curveB, kr, Z_R, Z_I)
end if
   
end subroutine snglnt1_F90


subroutine  dnpl1_F90(Z_R, Z_I, theta, RNS, NN, coord, k)

Use ie2mmex

implicit none

integer ::  NS, NN
real(wp) :: RNS ! Stockage MATLAB de NS
real(wp) :: Z_R(NN), Z_I(NN)
real(wp) :: coord(2,NN)
real(wp) :: theta
real(wp) :: k

type(curve) :: curveA

NS = int(RNS)
curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

call dnpl1_base(theta, curveA, k, Z_R, Z_I)

end subroutine dnpl1_F90

subroutine  dnpoint1_F90(Z_R, Z_I, X, RNS, NN, coord, k)

Use ie2mmex

implicit none

integer ::  NS, NN
real(wp) :: RNS ! Stockage MATLAB de NS
real(wp) :: Z_R(NN), Z_I(NN)
real(wp) :: coord(2,NN)
real(wp) :: X(2)
real(wp) :: k

type(curve) :: curveA

NS = int(RNS)
curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

call dnpoint1_base_r(X, curveA, k, Z_R, Z_I)

end subroutine dnpoint1_F90

subroutine  point1_F90(Z_R, Z_I, X, RNS, NN, coord, k)

Use ie2mmex

implicit none

integer ::  NS, NN
real(wp) :: RNS ! Stockage MATLAB de NS
real(wp) :: Z_R(NN), Z_I(NN)
real(wp) :: coord(2,NN)
real(wp) :: X(2)
real(wp) :: k

type(curve) :: curveA

NS = int(RNS)
curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

call point1_base_r(X, curveA, k, Z_R, Z_I)

end subroutine point1_F90

subroutine  dnpoint0_F90(Z_R, Z_I, X, NS, NN, coord, k)

Use ie2mmex

implicit none

integer ::  NS, NN
real(wp) :: Z_R(NS), Z_I(NS)
real(wp) :: coord(2,NN)
real(wp) :: X(2)
real(wp) :: k

type(curve) :: curveA

curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

call dnpoint0_base_r(X, curveA, k, Z_R, Z_I)

end subroutine dnpoint0_F90

subroutine  point0_F90(Z_R, Z_I, X, NS, NN, coord, k)

Use ie2mmex

implicit none

integer ::  NS, NN
real(wp) :: Z_R(NS), Z_I(NS)
real(wp) :: coord(2,NN)
real(wp) :: X(2)
real(wp) :: k

type(curve) :: curveA

curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

call point0_base_r(X, curveA, k, Z_R, Z_I)

end subroutine point0_F90

subroutine  dble1_F90(Z_R, Z_I, RNSA, NNA, coordA,    &
     &                          RNSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none

integer ::  NSA, NNA, NSB, NNB
real(wp) :: RNSA, RNSB ! Stockage MATLAB de NSA et NSB
real(wp) :: Z_R(NNA,NNB), Z_I(NNA,NNB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl

complex(wp) :: kc

type(curve) :: curveA, curveB

NSA = int(RNSA)
curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

NSB = int(RNSB)
curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB

kc = cmplx(kr,ki)

if(Iscpl) then
   call dble1_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call dble1_base_r(curveA, curveB, kr, Z_R, Z_I)
end if

end subroutine dble1_F90


subroutine  dble10_F90(Z_R, Z_I, NSA, NNA, coordA,    &
     &                           NSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none 

integer ::  NSA, NNA, NSB, NNB
real(wp) :: Z_R(NNA,NSB), Z_I(NNA,NSB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl


complex(wp) :: kc

type(curve) :: curveA, curveB

curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB

kc = cmplx(kr,ki)

if(Iscpl) then
   call dble10_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call dble10_base_r(curveA, curveB, kr, Z_R, Z_I)
end if


end subroutine dble10_F90


subroutine  dble01_F90(Z_R, Z_I, NSA, NNA, coordA,    &
     &                           NSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none 

integer ::  NSA, NNA, NSB, NNB
real(wp) :: Z_R(NSA,NNB), Z_I(NSA,NNB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl

complex(wp) :: kc

type(curve) :: curveA, curveB

curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB

kc = cmplx(kr,ki)

if(Iscpl) then
   call dble01_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call dble01_base_r(curveA, curveB, kr, Z_R, Z_I)
end if

end subroutine dble01_F90


subroutine  sngl1_F90(Z_R, Z_I, RNSA, NNA, coordA,    &
     &                          RNSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none

integer ::  NSA, NNA, NSB, NNB
real(wp) :: RNSA, RNSB ! Stockage MATLAB de NSA et NSB
real(wp) :: Z_R(NNA,NNB), Z_I(NNA,NNB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
complex(wp) :: kc 
logical  :: Iscpl

type(curve) :: curveA, curveB

NSA = int(RNSA)
curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

NSB = int(RNSB)
curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB

kc = cmplx(kr,ki) ! conversion en complexe

if(Iscpl) then
   call sngl1_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call sngl1_base_r(curveA, curveB, kr, Z_R, Z_I)
end if

end subroutine sngl1_F90


subroutine  mass1_F90(Z, RNSA, NNA, coordA)    

Use ie2mmex

implicit none

integer ::  NSA, NNA
real(wp) :: RNSA ! Stockage MATLAB de NSA et NSB
real(wp) :: Z(NNA,NNA)
real(wp) :: coordA(2,NNA)

type(curve) :: curveA

NSA = int(RNSA)
curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

call mass1_base(curveA, Z)

end subroutine mass1_F90


subroutine  mass_F90(Z, RNSA, NNA, coordA)    

Use ie2mmex

implicit none

integer ::  NSA, NNA
real(wp) :: RNSA ! Stockage MATLAB de NSA et NSB
real(wp) :: Z(NNA,NNA)
real(wp) :: coordA(2,NNA)

type(curve) :: curveA

NSA = int(RNSA)
curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA


call massdiag(curveA, Z)

end subroutine mass_F90


subroutine  mass0_F90(Z, NSA, NNA, coordA)    

Use ie2mmex

implicit none

integer ::  NSA, NNA
real(wp) :: Z(NSA,NSA)
real(wp) :: coordA(2,NNA)

type(curve) :: curveA

curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

call mass0_base(curveA, Z)

end subroutine mass0_F90

subroutine  plane1_F90(Z_R, Z_I, theta, RNS, NN, coord, k)

Use ie2mmex

implicit none

integer ::  NS, NN
real(wp) :: RNS ! Stockage MATLAB de NS
real(wp) :: Z_R(NN), Z_I(NN)
real(wp) :: coord(2,NN)
real(wp) :: theta
real(wp) :: k

type(curve) :: curveA

NS = int(RNS)
curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

call plane1_base(theta, curveA, k, Z_R, Z_I)

end subroutine plane1_F90


subroutine  plane0_F90(Z_R, Z_I, theta, NS, NN, coord, k)

Use ie2mmex

implicit none

integer ::  NS, NN
real(wp) :: Z_R(NS), Z_I(NS)
real(wp) :: coord(2,NN)
real(wp) :: theta
real(wp) :: k


type(curve) :: curveA

curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

call plane0_base(theta, curveA, k, Z_R, Z_I)

end subroutine plane0_F90


subroutine  dnpl0_F90(Z_R, Z_I, theta, NS, NN, coord, k)

Use ie2mmex

implicit none

integer ::  NS, NN
real(wp) :: Z_R(NS), Z_I(NS)
real(wp) :: coord(2,NN)
real(wp) :: theta
real(wp) :: k

!! temporary variables
complex(wp) :: Z

type(curve) :: curveA

curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

call dnpl0_base(theta, curveA, k, Z_R, Z_I)

end subroutine dnpl0_F90


subroutine farsngl1_F90(Z_R, Z_I, Ntheta, theta, J_R, J_I, & 
     &                              NS, NN, coord, k, Iscplx)

Use ie2mmex

implicit none

integer  ::  NS, NN, Ntheta
!real(wp) :: RNS ! Stockage MATLAB de NS
real(wp) :: Z_R(NTheta), Z_I(NTheta)
real(wp) :: J_R(NN), J_I(NN)
real(wp) :: coord(2,NN)
real(wp) :: theta(Ntheta)
real(wp) :: k
logical  :: Iscplx

type(curve) :: curveA
real(wp), pointer, dimension(:) :: JI


!NS = int(RNS)
curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

allocate(JI(NN))
if (Iscplx) then    
    JI = J_I
else
    JI = 0.    
endif 

call farsngl1_base(curveA, J_R, JI, k, theta, Z_R, Z_I)

end subroutine farsngl1_F90


subroutine fardble1_F90(Z_R, Z_I, Ntheta, theta, J_R, J_I, & 
     &                              RNS, NN, coord, k, Iscplx)

Use ie2mmex

implicit none

integer ::  NS, NN, Ntheta
real(wp) :: RNS ! Stockage MATLAB de NS
real(wp) :: Z_R(NTheta), Z_I(NTheta)
real(wp) :: J_R(NN), J_I(NN)
real(wp) :: coord(2,NN)
real(wp) :: theta(Ntheta)
real(wp) :: k
logical  :: Iscplx

real(wp), pointer, dimension(:) :: JI
type(curve) :: curveA

NS = int(RNS)
curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

allocate(JI(NN))
if (Iscplx) then    
    JI = J_I
else
    JI = 0.    
endif 


call fardble1_base(curveA, J_R, JI, k, theta, Z_R, Z_I)

end subroutine fardble1_F90


subroutine farsngl0_F90(Z_R, Z_I, Ntheta, theta, J_R, J_I, & 
     &                            NS, NN, coord, k, Iscplx)

Use ie2mmex

implicit none

integer ::  NS, NN, Ntheta
real(wp) :: Z_R(NTheta), Z_I(NTheta)
real(wp) :: J_R(NS), J_I(NS)
real(wp) :: coord(2,NN)
real(wp) :: theta(Ntheta)
real(wp) :: k
logical  :: Iscplx
real(wp), pointer, dimension(:) :: JI


type(curve) :: curveA

curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

allocate(JI(NS))
if (Iscplx) then    
    JI = J_I
else
    JI = 0.    
endif 

call farsngl0_base(curveA, J_R, JI, k, theta, Z_R, Z_I)

end subroutine farsngl0_F90


subroutine farterj00_F90(Z_R, Z_I, Ntheta, theta,  &
     &              J_R, J_I, rho_R, rho_I, NS, NN, coord, k, Iscplx)

Use ie2mmex

implicit none

integer ::  NS, NN, Ntheta
real(wp) :: Z_R(2,Ntheta), Z_I(2,Ntheta)
real(wp), pointer, dimension(:) :: Zx_R, Zx_I 
real(wp), pointer, dimension(:) :: Zy_R, Zy_I
real(wp) :: J_R(NS), J_I(NS), rho_R(Ns), rho_I(Ns)
real(wp) :: coord(2,NN)
real(wp) :: theta(Ntheta)
real(wp) :: k
logical  :: Iscplx
real(wp), pointer, dimension(:) :: JI, rhoI

type(curve) :: curveA

curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

allocate(JI(NS))
allocate(rhoI(Ns))
if (Iscplx) then    
    JI = J_I
    rhoI = rho_I
else
    JI = 0.
    rhoI = 0.    
endif 

allocate(Zx_R(Ntheta))
allocate(Zy_R(Ntheta))
allocate(Zx_I(Ntheta))
allocate(Zy_I(Ntheta))

call farterj00_base(curveA, J_R, JI, rho_R, rhoI, k, theta, &
                                     &   Zx_R, Zx_I, Zy_R, Zy_I)

Z_R(1,:) = Zx_R; Z_R(2,:) = Zy_R;
Z_I(1,:) = Zx_I; Z_I(2,:) = Zy_I;

end subroutine farterj00_F90



subroutine fardble0_F90(Z_R, Z_I, Ntheta, theta, J_R, J_I, & 
     &                            NS, NN, coord, k, Iscplx)

Use ie2mmex

implicit none

integer ::  NS, NN, Ntheta
real(wp) :: Z_R(NTheta), Z_I(NTheta)
real(wp) :: J_R(NS), J_I(NS)
real(wp) :: coord(2,NN)
real(wp) :: theta(Ntheta)
real(wp) :: k
logical :: Iscplx

real(wp), pointer, dimension(:) :: JI
type(curve) :: curveA

curveA%Ns = NS
allocate(curveA%coord(2,NN))
curveA%coord = coord

allocate(JI(NS))
if (Iscplx) then    
    JI = J_I
else
    JI = 0.    
endif

call fardble0_base(curveA, J_R, JI, k, theta, Z_R, Z_I)

end subroutine fardble0_F90


subroutine  sngl0_F90(Z_R, Z_I, NSA, NNA, coordA,    &
     &                           NSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none

integer ::  NSA, NNA, NSB, NNB
real(wp) :: Z_R(NSA,NSB), Z_I(NSA,NSB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl

type(curve) :: curveA, curveB
complex(wp) :: kc

curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA
kc = cmplx(kr,ki)

curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB


if(Iscpl) then
   call sngl0_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call sngl0_base_r(curveA, curveB, kr, Z_R, Z_I)
end if

end subroutine sngl0_F90

subroutine  sngl10_F90(Z_R, Z_I, NSA, NNA, coordA,    &
     &                           NSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none

integer ::  NSA, NNA, NSB, NNB
real(wp) :: Z_R(NSA,NSB), Z_I(NSA,NSB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl

type(curve) :: curveA, curveB
complex(wp) :: kc

curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA
kc = cmplx(kr,ki)

curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB


if(Iscpl) then
   call sngl10_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call sngl10_base_r(curveA, curveB, kr, Z_R, Z_I)
end if

end subroutine sngl10_F90



subroutine  drsngl0_F90(Z_R, Z_I, NSA, NNA, coordA,    &
     &                           NSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none

integer ::  NSA, NNA, NSB, NNB
real(wp) :: Z_R(NSA,NSB), Z_I(NSA,NSB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl

type(curve) :: curveA, curveB
complex(wp) :: kc

curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA
kc = cmplx(kr,ki)

curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB


if(Iscpl) then
   call drsngl0_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call drsngl0_base_r(curveA, curveB, kr, Z_R, Z_I)
end if

end subroutine drsngl0_F90


subroutine  dble0_F90(Z_R, Z_I, NSA, NNA, coordA,    &
     &                           NSB, NNB, coordB, kr, ki, Iscpl)

Use ie2mmex

implicit none

integer ::  NSA, NNA, NSB, NNB
real(wp) :: Z_R(NSA,NSB), Z_I(NSA,NSB)
real(wp) :: coordA(2,NNA), coordB(2,NNB)
real(wp) :: kr, ki
logical  :: Iscpl
complex(wp) :: kc

type(curve) :: curveA, curveB

curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

curveB%Ns = NSB
allocate(curveB%coord(2,NNB))
curveB%coord = coordB
kc = cmplx(kr,ki)

if(Iscpl) then
   call dble0_base_c(curveA, curveB, kc, Z_R, Z_I)
else
   call dble0_base_r(curveA, curveB, kr, Z_R, Z_I)
end if

end subroutine dble0_F90


subroutine  ds2p1_F90(Z_R, RNSA, NNA, coordA)

Use ie2mmex


implicit none

real(wp) :: RNSA
integer  :: NSA, NNA
real(wp) :: Z_R(NNA,NNA)
real(wp) :: coordA(2,NNA)


type(curve) :: curveA

NSA = int(RNSA)
curveA%Ns = NSA
allocate(curveA%coord(2,NNA))
curveA%coord = coordA

call ds2p1_base(Z_R, curveA)

end subroutine ds2p1_F90
















