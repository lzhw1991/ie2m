module ie2mcom

!!�������������  COMMON  DECLARATIONS  AND VARIABLES  �������

                                      !! Working precision
integer, parameter :: wp = kind(1.d0) !!       is
                                      !! double precision

real(wp), parameter :: pi = 3.141592653589793_wp


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Composite variable "curve" giving a meshed curve      !!
!!                                                         !!
!!      - curve%Ns : number of segments                    !!
!!      - curve%coord(2,N) : list of the nodes             !!
!!      - N = size(curve%coord,2)                          !!
!!                                                         !!
!!          when   N == Ns       "curve" is closed         !!
!!          when   N == Ns + 1   "curve" is open           !!
!!                                                         !!
type curve
   integer :: Ns
   real(wp), pointer, dimension(:,:) :: Coord 
end type curve
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!  Composite variable "segment" giving all data which can !!
!!  be involved when dealing with a segment of a meshed    !!
!!  curve                                                  !!
!!                                                         !!
!!     - segment%a(2)   : coordinates of the origin        !!
!!     - segment%b(2)   : coordinates of the vertex        !!
!!     - segment%m(2)   : coordinates of the mid-point     !!
!!     - segment%l      : length                           !!
!!     - segment%t(2)   : unit vector tangent to the       !!
!!                        segment                          !!
!!     - segment%n(2)   : unit normal to the segment       !!
!!                        obtained by turning tau by -pi/2 !!
!!                                                         !!
type segment
   real(wp) :: a(2), b(2), m(2), l, t(2), n(2)
end type segment
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Composite variable used enclosing the parameters        !!
!! required for the evaluation of the singular integrals   !!
type singint
   real(wp) :: RR1(2), RR2(2), R1, R2, LR1, LR2, RT1, RT2
   real(wp) :: THETA, D, L
end type singint
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

interface operator (.mp.)
   module procedure mulrc, mulcr, mulrr, mulcc
end interface


!!$interface ch0
!!$   module procedure ch0_r, ch0_c
!!$end interface
!!$
!!$interface chr0
!!$   module procedure chr0_r, chr0_c
!!$end interface
!!$
!!$interface ch1
!!$   module procedure ch1_r, ch1_c
!!$end interface
!!$
!!$interface chr1
!!$   module procedure chr1_r, chr1_c
!!$end interface


!!������� END of COMMON Declarations and variables ����������




contains


!************************************************************
!                                                           *
!                                                           *
!    P A R T  O F  T H E  C O D E  R E L A T I V E   T O    *
!          T H E   C O M M O N  U T I L I T I E S           *
!                                                           *
!                                                           *
!************************************************************

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: two real matrices
!!  output :: product of the two matrices
!!
function mulrr(u,v) result(sp)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
real(wp), intent(in) :: u(:,:), v(:,:)
real(wp) :: sp(size(u,1),size(v,2)) 
sp = matmul(u,v)
end function mulrr

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: two complex matrices
!!  output :: product of the two matrices
!!
function mulcc(u,v) result(sp)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
complex(wp), intent(in) :: u(:,:), v(:,:)
complex(wp) :: sp(size(u,1),size(v,2)) 
sp = matmul(u,v)
end function mulcc

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: a real and a complex matrix
!!  output :: product of the two matrices
!!
function mulrc(u,v) result(sp)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
real(wp), intent(in)    :: u(:,:)
complex(wp), intent(in) :: v(:,:)
complex(wp) :: sp(size(u,1),size(v,2)) 
sp = matmul(u,v)
end function mulrc

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: a complex and a real matrix
!!  output :: product of the two matrices
!!
function mulcr(u,v) result(sp)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
complex(wp), intent(in) :: u(:,:)
real(wp), intent(in)    :: v(:,:)
complex(wp) :: sp(size(u,1),size(v,2)) 
sp = matmul(u,v)
end function mulcr


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: two real vectors of length 2
!!  output :: scalar product
!!
function pscalair(u,v) result(sp)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
real(wp) :: u(2), v(2), sp 
sp = u(1)*v(1) + u(2)*v(2)
end function pscalair

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: vector with 2 real components
!!  output :: its length
!!
function rnorme(u) result(sp)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
real(wp) :: u(2), sp
sp = sqrt(pscalair(u,u))
end function rnorme

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: coordinates of two points in the plane
!!  output :: length of the segment joining this points
!!
real(wp) function rlong(a1,a2)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
real(wp) :: a1(2), a2(2)
rlong = rnorme(a2-a1)
end function rlong

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: coordinates of two points in the plane
!!  output :: variable of type segment giving data relative 
!!            to the segment with the two points as ends
!!
function ptoseg(a,b) result(sg)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
real(wp) :: a(2), b(2)
type(segment) :: sg
  sg%a = a; sg%b = b; sg%m = (a+b)/2;
  sg%l = rlong(a,b); sg%t = (b-a)/sg%l;
  sg%n(1) = sg%t(2); sg%n(2) = -sg%t(1);
end function ptoseg

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: composite variable "segment"
!!  output :: matrix expressing polynomial coefficients
!!            of a polynomial of degree at most 1 by
!!            its values at the ends of segment S
!!
function p1tond(S) result(P)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
type(segment), intent(in) :: S
real(wp) :: P(2,2)
P(1,:) = (/  1._wp, 0._wp /) 
P(2,:) = (/ -1._wp, 1._wp /)/S%l
end function p1tond




!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Hankel functions of the first kind
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

complex(wp) function ch0_r(k,R)
implicit none
real(wp), intent(in)    :: R
real(wp), intent(in)    :: k
!! local variables
real(wp) :: x, t, bj0, y0
complex(wp) :: arg
 
  if (abs(k) .le. 2*tiny(k)) then
     ch0_r = (0._wp,1._wp)*(2._wp/pi)*log(R);
     return
  end if

  x = k*R
  t = x/3
  if (abs(t) .le. 1._wp) then
     t = t**2
     bj0 = 0.0002100_wp
     bj0 = bj0*t - 0.0039444_wp
     bj0 = bj0*t + 0.0444479_wp
     bj0 = bj0*t - 0.3163866_wp
     bj0 = bj0*t + 1.2656208_wp
     bj0 = bj0*t - 2.2499997_wp
     bj0 = bj0*t + 1._wp

     y0 = -0.00024846_wp*t + 0.00427916_wp
     y0 = y0*t - 0.04261214_wp
     y0 = y0*t + 0.25300117_wp
     y0 = y0*t - 0.74350384_wp
     y0 = y0*t + 0.60559366_wp
     y0 = y0*t + 0.36746691_wp
     y0 = y0 + bj0*log(x/2)*(2/pi) 
     
     ch0_r = bj0 + (0._wp,1._wp)*y0

  else

     arg   = (0._wp,1._wp)*th0_r(x)
     ch0_r = f0_r(x)*exp(arg)/sqrt(x)

  end if

end function ch0_r

complex(wp) function ch0_c(k,R)
implicit none
real(wp), intent(in)    :: R
complex(wp), intent(in) :: k
!! local variables
complex(wp) :: x, t, bj0, y0
complex(wp) :: arg

  if (abs(k) .le. 2*tiny(1._wp)) then
     ch0_c = (0._wp,1._wp)*(2._wp/pi)*log(R);
     return
  end if
   
  x = k*R
  t = x/3
  if (abs(t) .le. 1._wp) then
     t = t**2
     bj0 = 0.0002100_wp
     bj0 = bj0*t - 0.0039444_wp
     bj0 = bj0*t + 0.0444479_wp
     bj0 = bj0*t - 0.3163866_wp
     bj0 = bj0*t + 1.2656208_wp
     bj0 = bj0*t - 2.2499997_wp
     bj0 = bj0*t + 1._wp

     y0 = -0.00024846_wp*t + 0.00427916_wp
     y0 = y0*t - 0.04261214_wp
     y0 = y0*t + 0.25300117_wp
     y0 = y0*t - 0.74350384_wp
     y0 = y0*t + 0.60559366_wp
     y0 = y0*t + 0.36746691_wp
     y0 = y0 + bj0*log(x/2)*(2/pi) 
     
     ch0_c = bj0 + (0.,1.)*y0

  else
   
     arg   = (0.,1.)*th0_c(x)
     ch0_c = f0_c(x)*exp(arg)/sqrt(x)

  end if

end function ch0_c


complex(wp) function chr0_r(k,R)
implicit none
real(wp), intent(in)    :: R
real(wp), intent(in)    :: k
!! local variables
real(wp) :: x, t, bj0, y0
 
  x = k*R
  t = x/3
  if (abs(t) .gt. 1._wp) call mexerrmsgtxt('Panic: argument too large in modified Bessel function of order 0')
     t = t**2
     bj0 = 0.0002100_wp
     bj0 = bj0*t - 0.0039444_wp
     bj0 = bj0*t + 0.0444479_wp
     bj0 = bj0*t - 0.3163866_wp
     bj0 = bj0*t + 1.2656208_wp
     bj0 = bj0*t - 2.2499997_wp
     bj0 = bj0*t + 1._wp
     if(abs(x) .eq. 0._wp) then
        y0=0.36746691_wp
     else
        y0 = -0.00024846_wp*t + 0.00427916_wp
        y0 = y0*t - 0.04261214_wp
        y0 = y0*t + 0.25300117_wp
        y0 = y0*t - 0.74350384_wp
        y0 = y0*t + 0.60559366_wp
        y0 = y0*t + 0.36746691_wp
        y0 = y0 + (bj0 -1._wp)*log(x/2)*(2/pi) 
     end if

     y0 = y0 + log(k/2)*(2/pi)     
     chr0_r = bj0 + (0._wp,1._wp)*y0

end function chr0_r

complex(wp) function chr0_c(k,R)
implicit none
real(wp), intent(in)    :: R
complex(wp), intent(in) :: k
!! local variables
complex(wp) :: x, t, bj0, y0

  x = k*R
  t = x/3
 
  if (abs(t) .gt. 1._wp) call mexerrmsgtxt('Panic: argument too large in modified Bessel function of order 0')
     t = t**2
     bj0 = 0.0002100_wp
     bj0 = bj0*t - 0.0039444_wp
     bj0 = bj0*t + 0.0444479_wp
     bj0 = bj0*t - 0.3163866_wp
     bj0 = bj0*t + 1.2656208_wp
     bj0 = bj0*t - 2.2499997_wp
     bj0 = bj0*t + 1._wp

     if(abs(x) .eq. 0._wp) then
        y0=0.36746691_wp
     else
        y0 = -0.00024846_wp*t + 0.00427916_wp
        y0 = y0*t - 0.04261214_wp
        y0 = y0*t + 0.25300117_wp
        y0 = y0*t - 0.74350384_wp
        y0 = y0*t + 0.60559366_wp
        y0 = y0*t + 0.36746691_wp
        y0 = y0 + (bj0-1._wp)*log(x/2)*(2/pi) 
     end if

     y0 = y0 + log(k/2)*(2/pi)
     chr0_c = bj0 + (0.,1.)*y0

end function chr0_c


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   Hankel functions of the first kind (order 1)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

complex(wp) function ch1_r(k,R)
implicit none
real(wp), intent(in)    :: R
real(wp), intent(in)    :: k
!! local variables
real(wp) :: x, t, bj1, y1
complex(wp) :: arg
 
  if (abs(k) .le. 2*tiny(k)) then
     ch1_r = 1._wp/R;
     return
  end if

  x = k*R
  t = x/3
  if (abs(t) .le. 1._wp) then
     t = t**2
     bj1 = 0.00001109_wp
     bj1 = bj1*t - 0.00031761_wp
     bj1 = bj1*t + 0.00443319_wp
     bj1 = bj1*t - 0.03954289_wp
     bj1 = bj1*t + 0.21093573_wp
     bj1 = bj1*t - 0.56249985_wp
     bj1 = (bj1*t + 0.5)*x

     y1 = 0.0027873_wp*t-0.0400976_wp
     y1 = y1*t + 0.3123951_wp
     y1 = y1*t - 1.3164827_wp
     y1 = y1*t + 2.1682709_wp
     y1 = y1*t + 0.2212091_wp
     y1 = y1*t - 0.6366198_wp
     y1 = y1/x + (2/pi)*log(x/2)*bj1
     
     ch1_r = bj1 + (0._wp,1._wp)*y1

  else

     arg   = (0._wp,1._wp)*th1_r(x)
     ch1_r = f1_r(x)*exp(arg)/sqrt(x)

  end if

end function ch1_r

complex(wp) function ch1_c(k,R)
implicit none
real(wp), intent(in)    :: R
complex(wp), intent(in) :: k
!! local variables
complex(wp) :: x, t, bj1, y1
complex(wp) :: arg


  if (abs(k) .le. 2*tiny(1._wp)) then
     ch1_c = 1._wp/R;
     return
  end if
 
  x = k*R
  t = x/3
  if (abs(t) .le. 1._wp) then
     t = t**2
     bj1 = 0.00001109_wp
     bj1 = bj1*t - 0.00031761_wp
     bj1 = bj1*t + 0.00443319_wp
     bj1 = bj1*t - 0.03954289_wp
     bj1 = bj1*t + 0.21093573_wp
     bj1 = bj1*t - 0.56249985_wp
     bj1 = (bj1*t + 0.5)*x

     y1 = 0.0027873_wp*t-0.0400976_wp
     y1 = y1*t + 0.3123951_wp
     y1 = y1*t - 1.3164827_wp
     y1 = y1*t + 2.1682709_wp
     y1 = y1*t + 0.2212091_wp
     y1 = y1*t - 0.6366198_wp
     y1 = y1/x + (2/pi)*log(x/2)*bj1

     ch1_c = bj1 + (0.,1.)*y1

  else
     
     arg   = (0.,1.)*th1_c(x)
     ch1_c = f1_c(x)*exp(arg)/sqrt(x)

  end if

end function ch1_c

complex(wp) function drch0_r(k,R)
implicit none
real(wp), intent(in)    :: R
real(wp), intent(in)    :: k
!! local variables
real(wp) :: x, t, bj1, y1
complex(wp) :: arg
 
  if (abs(k) .le. 2*tiny(k)) then
     call mexerrmsgtxt('Wavenumber equal to 0 in drsngl0')
     return
  end if

  x = k*R
  t = x/3
  if (abs(t) .le. 1._wp) then
     t = t**2
     bj1 = 0.00001109_wp
     bj1 = bj1*t - 0.00031761_wp
     bj1 = bj1*t + 0.00443319_wp
     bj1 = bj1*t - 0.03954289_wp
     bj1 = bj1*t + 0.21093573_wp
     bj1 = bj1*t - 0.56249985_wp
     bj1 = (bj1*t + 0.5)*x*R

     y1 = 0.0027873_wp*t-0.0400976_wp
     y1 = y1*t + 0.3123951_wp
     y1 = y1*t - 1.3164827_wp
     y1 = y1*t + 2.1682709_wp
     y1 = y1*t + 0.2212091_wp
     y1 = y1*t - 0.6366198_wp
     y1 = y1/k 
     
     if (abs(R) .gt. 2*tiny(R)) & 
        & y1 = y1 + (2/pi)*log(x/2)*bj1
             
     
     drch0_r = -(bj1 + (0._wp,1._wp)*y1)

  else

     arg   = (0._wp,1._wp)*th1_r(x)
     drch0_r = -R*f1_r(x)*exp(arg)/sqrt(x)

  end if


end function drch0_r


complex(wp) function drch0_c(k,R)
implicit none
real(wp), intent(in)    :: R
complex(wp), intent(in) :: k
!! local variables
real(wp) :: x, t, bj1, y1
complex(wp) :: arg
 
  if (abs(k) .le. 2*tiny(1._wp)) then
     call mexerrmsgtxt('Wavenumber equal to 0 in drsngl0')
     return
  end if

  x = k*R
  t = x/3
  if (abs(t) .le. 1._wp) then
     t = t**2
     bj1 = 0.00001109_wp
     bj1 = bj1*t - 0.00031761_wp
     bj1 = bj1*t + 0.00443319_wp
     bj1 = bj1*t - 0.03954289_wp
     bj1 = bj1*t + 0.21093573_wp
     bj1 = bj1*t - 0.56249985_wp
     bj1 = (bj1*t + 0.5)*x*R

     y1 = 0.0027873_wp*t-0.0400976_wp
     y1 = y1*t + 0.3123951_wp
     y1 = y1*t - 1.3164827_wp
     y1 = y1*t + 2.1682709_wp
     y1 = y1*t + 0.2212091_wp
     y1 = y1*t - 0.6366198_wp
     y1 = y1/k 
     
     if (abs(R) .gt. 2*tiny(R)) &
        & y1 = y1 + (2/pi)*log(x/2)*bj1
             
     
     drch0_c = -(bj1 + (0._wp,1._wp)*y1)

  else

     arg   = (0._wp,1._wp)*th1_r(x)
     drch0_c = -R*f1_r(x)*exp(arg)/sqrt(x)

  end if


end function drch0_c



complex(wp) function chr1_r(k,R)
implicit none
real(wp), intent(in)    :: R
real(wp), intent(in)    :: k
!! local variables
real(wp) :: x, t, bj1, y1

  if (abs(k) .le. 2*tiny(k)) then
     chr1_r = (0._wp,1._wp)*(2._wp/pi)/R;
     return
  end if
 
  x = k*R
  t = x/3
  
  if (abs(t) .gt. 1._wp) call mexerrmsgtxt('Panic: argument too large in modified Bessel function of order 1')
  
  if (abs(x) .eq. 0.d0) call mexerrmsgtxt('Panic: Argument equal to 0 in modified Neumann function of order 1')
     t = t**2
     bj1 = 0.00001109_wp
     bj1 = bj1*t - 0.00031761_wp
     bj1 = bj1*t + 0.00443319_wp
     bj1 = bj1*t - 0.03954289_wp
     bj1 = bj1*t + 0.21093573_wp
     bj1 = bj1*t - 0.56249985_wp
     bj1 = (bj1*t + 0.5)*x

     y1 = 0.0027873_wp*t-0.0400976_wp
     y1 = y1*t + 0.3123951_wp
     y1 = y1*t - 1.3164827_wp
     y1 = y1*t + 2.1682709_wp
     y1 = y1*t + 0.2212091_wp
     y1 = y1*t 
     y1 = y1/x + (2/pi)*log(x/2)*bj1 
    
     chr1_r = bj1 + (0._wp,1._wp)*y1

end function chr1_r

complex(wp) function chr1_c(k,R)
implicit none
real(wp), intent(in)    :: R
complex(wp), intent(in) :: k
!! local variables
complex(wp) :: x, t, bj1, y1
 
  x = k*R
  t = x/3
  
  if (abs(t) .gt. 1._wp) call mexerrmsgtxt('Panic: argument too large in modified Bessel function of order 1')
  
  if (abs(x) .eq. 0.d0) call mexerrmsgtxt('Panic: Argument equal to 0 in modified Neumann function of order 1' ) 
     t = t**2
     bj1 = 0.00001109_wp
     bj1 = bj1*t - 0.00031761_wp
     bj1 = bj1*t + 0.00443319_wp
     bj1 = bj1*t - 0.03954289_wp
     bj1 = bj1*t + 0.21093573_wp
     bj1 = bj1*t - 0.56249985_wp
     bj1 = (bj1*t + 0.5)*x

     y1 = 0.0027873_wp*t-0.0400976_wp
     y1 = y1*t + 0.3123951_wp
     y1 = y1*t - 1.3164827_wp
     y1 = y1*t + 2.1682709_wp
     y1 = y1*t + 0.2212091_wp
     y1 = y1*t 
     y1 = y1/x + (2/pi)*log(x/2)*bj1
    
     chr1_c = bj1 + (0._wp,1._wp)*y1

end function chr1_c



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!  Auxiliary functions for the determination of Bessel
!!  and Neumann of order 0 and 1
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

real(wp) Function f0_r(x)
implicit none
real(wp) :: x
!! local variable
real(wp) :: t

  t    = 3/x
  f0_r = 0.00014476_wp*t -0.00072805_wp
  f0_r = f0_r*t + 0.00137237_wp
  f0_r = f0_r*t - 0.00009512_wp
  f0_r = f0_r*t - 0.00552740_wp
  f0_r = f0_r*t - 0.00000077_wp
  f0_r = f0_r*t + 0.79788456_wp

end Function f0_r

complex(wp) Function f0_c(x)
implicit none
complex(wp) :: x
!! local variable
complex(wp) :: t

  t    = 3/x
  f0_c = 0.00014476_wp*t -0.00072805_wp
  f0_c = f0_c*t + 0.00137237_wp
  f0_c = f0_c*t - 0.00009512_wp
  f0_c = f0_c*t - 0.00552740_wp
  f0_c = f0_c*t - 0.00000077_wp
  f0_c = f0_c*t + 0.79788456_wp

end Function f0_c


real(wp) Function th0_r(x)
implicit none
real(wp) :: x
!! local variable
real(wp) :: t

  t     = 3/x
  th0_r = 0.00013558_wp*t -0.00029333_wp
  th0_r = th0_r*t - 0.00054125_wp
  th0_r = th0_r*t + 0.00262573_wp
  th0_r = th0_r*t - 0.00003954_wp
  th0_r = th0_r*t - 0.04166397_wp
  th0_r = th0_r*t - 0.78539816_wp + x

end Function th0_r

complex(wp) Function th0_c(x)
implicit none
complex(wp) :: x
!! local variable
complex(wp) :: t

  t      = 3/x
  th0_c  = 0.00013558_wp*t -0.00029333_wp
  th0_c  = th0_c*t - 0.00054125_wp
  th0_c  = th0_c*t + 0.00262573_wp
  th0_c  = th0_c*t - 0.00003954_wp
  th0_c  = th0_c*t - 0.04166397_wp
  th0_c  = th0_c*t - 0.78539816_wp + x

end Function th0_c



real(wp) Function f1_r(x)
real(wp) x
!! local variable
real(wp) :: t

 t = 3/x
 f1_r = -0.00020033_wp*t +0.00113653_wp
 f1_r = f1_r*t - 0.00249511_wp
 f1_r = f1_r*t + 0.00017105_wp
 f1_r = f1_r*t + 0.01659667_wp
 f1_r = f1_r*t + 0.00000156_wp
 f1_r = f1_r*t + 0.79788456_wp

end Function f1_r

complex(wp) Function f1_c(x)
complex(wp) x
!! local variable
complex(wp) :: t

 t    = 3/x
 f1_c = -0.00020033_wp*t +0.00113653_wp
 f1_c = f1_c*t - 0.00249511_wp
 f1_c = f1_c*t + 0.00017105_wp
 f1_c = f1_c*t + 0.01659667_wp
 f1_c = f1_c*t + 0.00000156_wp
 f1_c = f1_c*t + 0.79788456_wp

end Function f1_c

real(wp) Function th1_r(x)
real(wp) x
!! local variable
real(wp) :: t

 t     = 3/x
 th1_r = -0.00029166_wp*t + 0.00079824_wp
 th1_r = th1_r*t + 0.00074348_wp
 th1_r = th1_r*t - 0.00637879_wp
 th1_r = th1_r*t + 0.00005650_wp
 th1_r = th1_r*t + 0.12499612_wp
 th1_r = th1_r*t - 2.35619449_wp + x

end Function th1_r

complex(wp) Function th1_c(x)
complex(wp) x
!! local variable
complex(wp) :: t

 t     = 3/x
 th1_c = -0.00029166_wp*t + 0.00079824_wp
 th1_c = th1_c*t + 0.00074348_wp
 th1_c = th1_c*t - 0.00637879_wp
 th1_c = th1_c*t + 0.00005650_wp
 th1_c = th1_c*t + 0.12499612_wp
 th1_c = th1_c*t - 2.35619449_wp + x

end Function th1_c

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                          
!!  input  :: order of the numerical quadrature
!!  output :: array with barycentric coordinates and weight
!!            of the quadrature formula
!!      - GAUSS(1:2) : barycentric coordinates
!!      - GAUSS(3)   : weight
!!
function setquad(N) result(GAUSS)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
integer, intent(in) :: N
real(wp) :: GAUSS(3,N)
! local variables
real(wp), dimension(N) :: XTAB, WEIGHT

    select case (N)
    case (1) 

      XTAB( 1) =  5.0000000000000000D-01
  
      WEIGHT( 1) =  1.0000000000000000D+00
  
    case (2) 
  
      XTAB( 1) =  2.1132487058639526D-01
      XTAB( 2) =  7.8867512941360474D-01
  
      WEIGHT( 1) =  5.0000000000000000D-01
      WEIGHT( 2) =  5.0000000000000000D-01
  
    case (3)
  
      XTAB( 1) =  1.1270165443420410D-01
      XTAB( 2) =  5.0000000000000000D-01
      XTAB( 3) =  8.8729834556579590D-01
  
      WEIGHT( 1) =  2.7777777777777779D-01
      WEIGHT( 2) =  4.4444444444444442D-01
      WEIGHT( 3) =  2.7777777777777779D-01
    case default
       write(*,*) 'Panic: Fatal error in Numerical Quadrature'
       write(*,*) 'Illegal value for the number of nodes = ', N
       write(*,*) 'Valid values are 1 to 16, 20, 32, 64'
    end select

GAUSS(2,:) = XTAB
GAUSS(1,:) = 1._wp - XTAB
GAUSS(3,:) = WEIGHT

end function setquad

!************************************************************
!                                                           *
!                                                           *
!    P A R T  O F  T H E  C O D E  R E L A T I V E   T O    *
!          T H E  C O M P U T A T I O N  O F  T H E         *
!                 B A S I C  I N T E G R A L S              *
!                                                           *
!                                                           *
!************************************************************


function G0_r(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  real(wp), intent(in)      :: kwave
  integer, parameter :: N = 2
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), X(2), Y(2)
  real(wp) :: R
  integer  :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T); 
        G(I,J) = ch0_r(kwave,R)*GAUSS(3,I)*GAUSS(3,J)
     end do
  end do

end function G0_r

function G0_c(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  complex(wp), intent(in)      :: kwave
  integer, parameter :: N = 2
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp)    :: T(2), X(2), Y(2)
  real(wp)    :: R
  complex(wp) :: kR  
  integer     :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T); 
        G(I,J) = ch0_c(kwave,R)*GAUSS(3,I)*GAUSS(3,J)
     end do
  end do

end function G0_c

function DRG0_r(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  real(wp), intent(in)      :: kwave
  integer, parameter :: N = 2
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), X(2), Y(2)
  real(wp) :: R
  integer  :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T); 
        G(I,J) = drch0_r(kwave,R)*GAUSS(3,I)*GAUSS(3,J)
     end do
  end do

end function DRG0_r

function DRG0_c(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  complex(wp), intent(in)      :: kwave
  integer, parameter :: N = 2
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp)    :: T(2), X(2), Y(2)
  real(wp)    :: R
  complex(wp) :: kR  
  integer     :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T); 
        G(I,J) = drch0_c(kwave,R)*GAUSS(3,I)*GAUSS(3,J)
     end do
  end do

end function DRG0_c


function GR0_r(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  real(wp), intent(in)      :: kwave
  integer, parameter :: N = 3
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), X(2), Y(2)
  real(wp) :: R
  integer  :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T);
        G(I,J) = chr0_r(kwave,R)*GAUSS(3,I)*GAUSS(3,J)
     end do
  end do

end function GR0_r

function GR0_c(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  complex(wp), intent(in)      :: kwave
  integer, parameter :: N = 3
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp)    :: T(2), X(2), Y(2)
  real(wp)    :: R
  integer     :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T); 
        G(I,J) = chr0_c(kwave,R)*GAUSS(3,I)*GAUSS(3,J)
     end do
  end do

end function GR0_c

function G1_r(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  real(wp), intent(in)      :: kwave
  integer, parameter :: N = 2
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), X(2), Y(2)
  real(wp) :: R
  integer  :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T); 
        G(I,J) = ch1_r(kwave,R)*pscalair(T,SL%n) &
                               *GAUSS(3,I)*GAUSS(3,J)/R
     end do
  end do

end function G1_r

function GX1_r(X,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SL
  real(wp), intent(in)      :: kwave
  real(wp), intent(in)      :: X(2)
  integer, parameter :: N = 2
  complex(wp) :: G(N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), Y(2)
  real(wp) :: R
  integer  :: J

  GAUSS = setquad(N)
  do J=1,N
     Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
     T = Y - X; R = rnorme(T); 
     G(J) = ch1_r(kwave,R)*pscalair(T,SL%n)/R
     G(J) = G(J)*GAUSS(3,J)
  end do

end function GX1_r


function GX0_r(X,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SL
  real(wp), intent(in)      :: kwave
  real(wp), intent(in)      :: X(2)
  integer, parameter :: N = 2
  complex(wp) :: G(N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), Y(2)
  real(wp) :: R
  integer  :: J

  GAUSS = setquad(N)
  do J=1,N
     Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
     T = Y - X; R = rnorme(T); 
     G(J) = ch0_r(kwave,R)
     G(J) = G(J)*GAUSS(3,J)
  end do

end function GX0_r


function G1_c(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  complex(wp), intent(in)      :: kwave
  integer, parameter :: N = 2
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), X(2), Y(2)
  real(wp) :: R
  integer  :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T); 
        G(I,J) = ch1_c(kwave,R)*pscalair(T,SL%n) &
                               *GAUSS(3,I)*GAUSS(3,J)/R
     end do
  end do

end function G1_c

function GR1_r(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  real(wp), intent(in)      :: kwave
  integer, parameter :: N = 3
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), X(2), Y(2)
  real(wp) :: R
  integer  :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T);
        G(I,J) = chr1_r(kwave,R)*pscalair(T,SL%n) &
                               *GAUSS(3,I)*GAUSS(3,J)/R
     end do
  end do

end function GR1_r

function GR1_c(SK,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SK, SL
  complex(wp), intent(in)      :: kwave
  integer, parameter :: N = 3
  complex(wp) :: G(N,N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), X(2), Y(2)
  real(wp) :: R
  integer  :: I, J

  GAUSS = setquad(N)
  do I=1,N
     X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
     do J=1,N
        Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
        T = Y - X; R = rnorme(T); 
        G(I,J) = chr1_c(kwave,R)*pscalair(T,SL%n) &
                               *GAUSS(3,I)*GAUSS(3,J)/R
     end do
  end do

end function GR1_c

function GRX1_r(X,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SL
  real(wp), intent(in)      :: kwave
  real(wp), intent(in)      :: X(2)
  integer, parameter :: N = 3
  complex(wp) :: G(N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), Y(2)
  real(wp) :: R
  integer  :: J

  GAUSS = setquad(N)
 
 
  do J=1,N
     Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
     T = Y - X; R = rnorme(T);
     G(J) = chr1_r(kwave,R)*pscalair(T,SL%n)*GAUSS(3,J)/R
  end do
 

end function GRX1_r

function GRX0_r(X,SL,kwave) result(G)
  implicit none
  type(segment), intent(in) :: SL
  real(wp), intent(in)      :: kwave
  real(wp), intent(in)      :: X(2)
  integer, parameter :: N = 3
  complex(wp) :: G(N)
!! Local variables
  real(wp), dimension(3,N) :: GAUSS 
  real(wp) :: T(2), Y(2)
  real(wp) :: R
  integer  :: J

  GAUSS = setquad(N)
 
 
  do J=1,N
     Y = GAUSS(1,J)*SL%a + GAUSS(2,J)*SL%b
     T = Y - X; R = rnorme(T);
     G(J) = chr0_r(kwave,R)*GAUSS(3,J)
  end do
 

end function GRX0_r

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! function giving the integral for a non singular kernel G 
!! for two segments "SK" and "SL"
!!  
!!   Z = \int_SK \int_SL G(k,x,y) s^l t^m dL(y) dK(x)
!!   
!!   k /= 0 : G is the kernel relative to Helmholtz equation
!!            either for the single or the double-layer
!!            
!!   k = 0  : if fact (abs(k) <= tiny) kernel G is relative 
!!            to the the Laplace equation : 
!!                  - (1/2\pi) log |x-y|
!!
!! Input  : SK, SL  variables of type segment
!!        : l,  m   degrees of monomials on respectively
!!                  SK and SL
!!        : k       wavenumber
!!        : Np      number of points of the 
!!                  quadrature formula.
!!        : G       kernel weighted by the weights involved
!!                  in the used Gauss quadrature formula 
!! 
!! output : Z 
!!                  
function intnum(SK,l,SL,m,G,Np) result(Z)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
type(segment), intent(in) :: SK, SL
integer      , intent(in) :: l,  m
integer      , intent(in) :: Np
complex(wp)  , intent(in) :: G(Np,Np)
complex(wp) :: Z
! local variables
real(wp),  dimension(3,Np) :: GAUSS ! see setquad
real(wp) :: si, tj
integer  :: I, J
complex(wp) :: ZI


GAUSS = setquad(Np)

Z = 0._wp ! initialization

do I=1,Np
   si = (GAUSS(2,I)*SK%l)**l
   ZI = 0._wp ! Initialization of the sum on J
   do J=1,Np
      tj = (GAUSS(2,J)*SL%l)**m
      ZI = ZI + G(I,J)*tj
   end do
   Z = Z + ZI*si
end do

Z = Z*SK%l*SL%l

end function intnum

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! function giving the integral for a non singular kernel G 
!! for a point x and a segment "SL"
!!  
!!   Z = \int_SL G(k,x,y) t^m dL(y) dK(x)
!!   
!!   k /= 0 : G is the kernel relative to Helmholtz equation
!!            either for the single or the double-layer
!!            
!!   k = 0  : if fact (abs(k) <= tiny) kernel G is relative 
!!            to the the Laplace equation : 
!!                  - (1/2\pi) log |x-y|
!!
!! Input  : SL  variable of type segment
!!        : X(2) the coordinates of X
!!        : l,  m   degrees of monomials on respectively
!!                  SK and SL
!!        : k       wavenumber
!!        : Np      number of points of the 
!!                  quadrature formula.
!!        : G       kernel weighted by the weights involved
!!                  in the used Gauss quadrature formula 
!! 
!! output : Z 
!!                  
function intnumx(SL,m,G,Np) result(Z)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
type(segment), intent(in) :: SL
integer      , intent(in) :: m
integer      , intent(in) :: Np
complex(wp)  , intent(in) :: G(Np)
complex(wp) :: Z
! local variables
real(wp),  dimension(3,Np) :: GAUSS ! see setquad
real(wp) :: tj
integer  :: J


GAUSS = setquad(Np)

Z = 0._wp ! initialization

   do J=1,Np
      tj = (GAUSS(2,J)*SL%l)**m
      Z = Z + G(J)*tj
   end do

Z = Z*SL%l

end function intnumx



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! function giving the singular part of a single layer 
!! potential for two near segments "SK" and "SL"
!!  
!! The kernel is decomposed in :
!!     - (-1/2*pi) log(|x-y|) incorporating the
!!       singular part of the integral
!!     - (i/4) CH0(k|x-y|)
function intsgl_near(SK,l,SL,m,Np) result(Z)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
type(segment), intent(in) :: SK, SL
integer      , intent(in) :: l,  m
integer      , intent(in) :: Np
real(wp)                  :: Z  
! local variables
real(wp), allocatable, dimension(:,:) :: GAUSS ! see setquad
real(wp) :: T(2), X(2), Y(2)
real(wp) :: R
real(wp) :: si, tj
integer  :: I, J
type(singint) :: VX

allocate(GAUSS(3,Np))
GAUSS = setquad(Np)

Z = 0._wp ! initialization

do I=1,Np
   X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
   si = (GAUSS(2,I)*SK%l)**l
   VX = prsingint(X,SL)
   Z = Z - singsgl(VX,m,GAUSS)*si*GAUSS(3,I) ! adding the singular part 
end do

Z = Z*SK%l*0.5/pi

end function intsgl_near

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! function giving the singular part of a single layer 
!! potential for two near segments "SK" and "SL"
!!  
!! The kernel is decomposed in :
!!     - (-1/2*pi) log(|x-y|) incorporating the
!!       singular part of the integral
!!     - (i/4) CH0(k|x-y|)
function intsglx_near(X,SL,m,Np) result(Z)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
type(segment), intent(in) :: SL
integer      , intent(in) :: m
integer      , intent(in) :: Np
real(wp)     , intent(in) :: X(2)
real(wp)                  :: Z  
! local variables
real(wp), allocatable, dimension(:,:) :: GAUSS ! see setquad
real(wp) :: T(2), Y(2)
real(wp) :: R
real(wp) :: si, tj
integer  :: I, J
type(singint) :: VX

allocate(GAUSS(3,Np))
GAUSS = setquad(Np)

Z = 0._wp ! initialization

   VX = prsingint(X,SL)
   Z = Z - singsgl(VX,m,GAUSS) ! adding the singular part 

Z = Z*0.5/pi

end function intsglx_near

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! function giving the singular part of a double layer 
!! potential for two near segments "SK" and "SL"
!!  
!! see intsgl_far   
!!
!! The kernel is decomposed in :
!!     - (1/2*pi) grad_SL%n log(|x-y|) incorporating the
!!       singular part of the integral
!!     - (ik/4) CH1 (y-x)/|y-x|.SL%n
function intdbl_near(SK,l,SL,m,Np) result(Z)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
type(segment), intent(in) :: SK, SL
integer      , intent(in) :: l,  m
integer      , intent(in) :: Np
real(wp)                  :: Z
! local variables
real(wp), dimension(3,Np) :: GAUSS ! see setquad
real(wp) :: T(2), X(2)
real(wp) :: si, tj
integer  :: I, J
type(singint) :: VX
real(wp) :: d1, d2, dmin

GAUSS = setquad(Np)

Z = 0._wp ! initialization

!! test if SK = SL
d1 = rlong(SK%a,SL%a)+rlong(SK%b,SL%b)
d2 = rlong(SK%a,SL%b)+rlong(SK%b,SL%a)
dmin = min(d1,d2)

if(2*dmin/(SK%l+SL%l) .le. epsilon(dmin)) return

do I=1,Np
   X = GAUSS(1,I)*SK%a + GAUSS(2,I)*SK%b
   si = (GAUSS(2,I)*SK%l)**l
   VX = prsingint(X,SL)
   Z = Z + singdbl(VX,m,GAUSS)*si*GAUSS(3,I) ! adding the singular part  
end do

Z = Z*SK%l*0.5/pi

end function intdbl_near


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! function giving the singular part of the integral relative
!! to a double layer potential for a point X  
!! and a segment "SL"
!!  
!! see intsgl_far   
!!
!! The kernel is decomposed in :
!!     - (1/2*pi) grad_SL%n log(|x-y|) incorporating the
!!       singular part of the integral
!!     - (ik/4) CH1 (y-x)/|y-x|.SL%n
function intdblx_near(X,SL,m,Np) result(Z)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
type(segment), intent(in) :: SL
integer      , intent(in) :: m
integer      , intent(in) :: Np
real(wp)     , intent(in) :: X(2)
real(wp)                  :: Z
! local variables
real(wp), dimension(3,Np) :: GAUSS ! see setquad
real(wp) :: tj
integer  :: J
type(singint) :: VX

GAUSS = setquad(Np)

Z = 0._wp ! initialization


VX = prsingint(X,SL)
Z = Z + singdbl(VX,m,GAUSS) ! adding the singular part  

Z = Z*0.5/pi

end function intdblx_near


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! function preparing the data used by the computation of
!! the singular integral
!!
!! input  : X, SL (see function singsgl)
!! output : composite variable VX
!!         
!!    VX%RR1 = SL%a - X; VX%R1 = |RR1|; VX%LR1 = log(R1)
!!    VX%RR2 = SL%b - X; VX%R2 = |RR2|; VX%LR2 = log(R2)
!!    VX%RT1 = RR1.SL%t; VX%RT2 = RR2.SL%t
!!    VX%THETA = (signed) angle of (RR1,RR2)
!!    VX%D     = (signed) distance of X to line (SL%a,S%b)
function prsingint(X,SL) result(VX)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
real(wp), intent(in)      :: X(2)
type(segment), intent(in) :: SL
type(singint) :: VX
! local variable
real(wp) :: PS

      VX%RR1 = SL%a - X; VX%R1 = rnorme(VX%RR1);
      if(VX%R1 .le. tiny(VX%R1)) call mexerrmsgtxt('Panic: Severe error in the computation of singular integral')  
      VX%RT1 = pscalair(VX%RR1,SL%t)
      VX%LR1 = log(VX%R1)
      VX%RR2 = SL%b - X; VX%R2 = rnorme(VX%RR2);
      if(VX%R2 .le. tiny(VX%R2)) call mexerrmsgtxt('Panic: Argument equal to 0 in modified Neumann function of order 1' )
      VX%RT2 = pscalair(VX%RR2,SL%t)
      VX%LR2 = log(VX%R2)
      VX%D = pscalair(SL%n,VX%RR1)
      PS = pscalair(VX%RR1/VX%R1,VX%RR2/VX%R2)
      if(PS .lt. -1._wp + 2*epsilon(PS)) then
         VX%THETA = pi
      elseif(PS .gt. 1._wp - 2*epsilon(PS)) then
         VX%THETA = 0._wp
      else
         VX%THETA = acos(PS)
      end if
      
      VX%THETA = sign(VX%THETA,VX%D)
      VX%l = SL%l
end function prsingint

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! auxiliary function giving a primitive of function
!!  R log(R)
!!
!! input : positive variable R
!!
function rlgr(R) result(S)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
real(wp), intent(in) :: R
real(wp)             :: S
!! local variables

if(R .le. -tiny(R)) call mexerrmsgtxt('Panic: severe error in the computation of singular integrals')
 

if(R .le. tiny(R)) then
   S = 0._wp
else
   S = 0.5*R**2*(log(R) - 0.5_wp)
end if
end function rlgr

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! recursive function giving the integral of the singularity
!! relative to a single layer integral  
!! 
!! Z =  \int_SL log(|x-y|) t^m dL(y)
!! 
!! input : VX  data needed for the computation of singular 
!!             integral 
!!         m   degree of the monomial on SL
!!         GAUSS  quadrature points and weights for the
!!                regular parts of the integral for m > 1
recursive function singsgl(VX,m,GAUSS) result(Z)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
type(singint), intent(in) :: VX
real(wp),      intent(in) :: GAUSS(:,:)
integer,       intent(in) :: m
real(wp)                  :: Z
! local variables
real(wp) :: GR, R, ti
integer  :: I

select case (m)
   case (0)
      Z = VX%RT2*VX%LR2 - VX%RT1*VX%LR1 - VX%l + VX%D*VX%THETA
   case (1)
      GR = rlgr(VX%R2) - rlgr(VX%R1)
      Z = GR - VX%RT1*singsgl(VX,0,GAUSS)
   case (2:)
      GR = 0._wp
      do I=1,size(GAUSS,2)
         R = rnorme(GAUSS(1,I)*VX%RR1+GAUSS(2,I)*VX%RR2)
         ti = (GAUSS(2,I)*VX%l)**(m-2)
         GR = GR + GAUSS(3,I)*ti*rlgr(R)
      end do
      GR = VX%l**(m-1)*rlgr(VX%R2) - (m-1)*VX%l*GR
      Z = GR - VX%RT1*singsgl(VX,m-1,GAUSS)
   case default
      call mexerrmsgtxt('Panic: Severe error in the singular integral')  
end select
end function singsgl


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! recursive function giving the integral of the singularity
!! relative to a single layer integral  
!! 
!! Z =  \int_SL (1/|y-x|)(y-x)|y-x|.SL%n t^m dL(y)
!! 
!! input : VX  data needed for the computation of singular 
!!             integral 
!!         m   degree of the monomial on SL
!!         GAUSS  quadrature points and weights for the
!!                regular parts of the integral for m > 1
recursive function singdbl(VX,m,GAUSS) result(Z)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
implicit none
type(singint), intent(in) :: VX
integer,       intent(in) :: m
real(wp),      intent(in) :: GAUSS(:,:)
real(wp)                  :: Z

select case (m)
   case (0)
      Z = VX%THETA
   case (1)
      Z = VX%D*log(VX%R2/VX%R1) - VX%RT1*VX%THETA
   case (2:)
      Z = VX%D*(VX%LR2*VX%l**(m-1)-(m-1)*singsgl(VX,m-2,GAUSS)) &
                                - VX%RT1*singdbl(VX,m-1,GAUSS)
   case default
      call mexerrmsgtxt( 'Panic: Severe error in the singular integral')  
end select
end function singdbl


!=================================================================
!                                                                =
!           End of the part related to the computation of        =
!                        basic integrals                         =
!                                                                =
!=================================================================



end module ie2mcom









