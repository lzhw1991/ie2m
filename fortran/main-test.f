    
      program test

      IMPLICIT NONE

      INTEGER NNA, NNB, NSA, NSB
      REAL*8 Z_R(10,10), Z_I(10,10)
      REAL*8 coordA(2,10), coordB(2,10)
      COMPLEX*16 kr
      REAL*8 ki
      LOGICAL Iscpl

      INTEGER ii, jj
      COMPLEX*16 Z(10,10)

Cf2py intent(in) NSA, NSB, kr
Cf2py intent(in) coordA, coordB
Cf2py intent(out) Z
Cf2py integer intent(hide),depend(coordA) :: NNA=shape(coordA,1)
Cf2py integer intent(hide),depend(coordB) :: NNB=shape(coordB,1)
Cf2py real intent(hide) :: ki
Cf2py logical intent(hide) :: Iscpl

Cf2py external dble01_F90


      print*, '== enter simple test =='
      print*, '  this checks nothing!'
      print*, '  verify the sanity of the lib building'
      print*, '  only: call dble01_F90'

      kr = (0.0,0.0)
      nsa = 5
      nsb = 5
      nna = 5
      nnb = 5
      

      IF (imag(kr) .NE. 0.0) THEN
         ki = imag(kr)
         Iscpl = .true.
      ELSE
         ki = 0.0
         Iscpl = .false.
      END IF

      CALL dble01_F90(Z_R, Z_I, NSA, NNA, coordA,
     1                              NSB, NNB, coordB,
     2                              kr, ki, Iscpl)

      do ii=1,NSA
         do jj=1,NNB
            Z(ii,jj) =  (1.0,0.0)* Z_R(ii,jj) + (0.0,1.0)* Z_I(ii,jj)
         enddo
      enddo


      print*, '  did it.'
      print*, 'Try the binding with python.'
      print*, '== exit simple test =='

      end program test
