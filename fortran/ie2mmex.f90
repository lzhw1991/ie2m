module ie2mmex

!============================================================
!! copyright A. BENDALI, MIP laboratory and CERFACS         *
!!           Toulouse (France)                              *
!!----------------------------------------------------------*
!!                                                          *
!! Last revision: XXXXXXXXXXXXXX                            *
!!----------------------------------------------------------*
!! Please report bugs and problems to                       *
!!    bendali@gmm.insa-tlse.fr                              *
!============================================================

!************************************************************
!                                                           *
!  Library of routines to solve scattering problems in      *
!  electromagnetism in 2d Approximation both in TE and      * 
!  TH cases                                                 *
!                                                           *
!  The basic routines form the matrices related to          *
!                                                           *
!     - a single layer potential                            *
!     - a double-layer potential                            *
!     - the normal trace of a double-layer potential        *
!                                                           *
!  The unknown function and the testing function are        *
!  defined on two curves either closed or open and which    *
!  can be identical or different.                           *
!                                                           *
!  Each the data relative to curve is structured in a       *
!  composite variable, described below, including in        *
!  particular the list of the nodes.                        *
!                                                           *
!  This list consists of a successive numbering of the      *
!  nodes in a fixed way on the curve. Each element is       *
!  implicitely given by two successive nodes.               *
!                                                           *
!  The unit normal is obtained by normalizing the vector    *
!  that origin is the first node of the element (segment)   *
!  and the apex is the second one and turning it by \pi/2   *
!  in the counterclockwise direction.                       *
!                                                           *
!  There are also routines computing the field patterns for *
!     - a single layer potential supported on a given curve *
!     - a double layer potential in the same case           *
!     - the field pattern of a line of electric currents in *
!   the TH case                                             *
!     - the field pattern of a line of magnetic currents in *
!   the TE case                                             *
!************************************************************


use ie2mcom



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!           COMMON VARIABLES                              !!
!!                                                         !!
!!  - D : relative distance of the mid-points of           !!
!!        two segments under which the singularity         !!
!!        is extracted and exactly integrated              !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

real(wp), parameter :: D = 0.75_wp    

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!     Declarations of generic calls                       !!
!!$interface farpoint_base ! see how it works below
!!$   module procedure farpoint_vector, farpoint_scalar
!!$end interface
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains

!************************************************************
!                                                           *
!                                                           *
!    P A R T  O F  T H E  C O D E  R E L A T I V E   T O    *
!           T H E  C O M P U T A T I O N  O F               *
!         M A T R I C E S  A S S O C I A T E D  T O         *
!       V A L U E S  O F   P O T E N T I A L S  A N D       *
!    A N D  T H E I R  N O R M A L  D E R I V A T I V E     *
!                                                           *
!                                                           *
!    In principle, the solution of any standard radiation   *
!    or scattering 2D problem can be obtained from a simple *
!    combination of these matrices                          *
!************************************************************


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function sngl0 returns the matrix relative to the     !!
!!   bilinear form associated to the value of a single     !!
!!   layer potential created by a density \lamda on a      !!
!!   curve A on a curve B                                  !!
!!                                                         !!
!! \int_A \int_B G(x,y) \lambda(y) \lambda'(x) dB(y) dA(x) !!
!!                                                         !!
!!  = [\lambda']^T [snly] [\lambda]                        !!
!!                                                         !!
!!     A and B : two curves meshed in segments             !!
!!        \lambda : generic function constant per segment  !!
!!                                                         !!               
!!        \lambda': generic function (of the same type     !!
!!                  than \lambda)                          !!
!!                                                         !!
!!        [\lambda]  : column-vector that components are   !!
!!     the values of \lambda on each segment               !!
!!        [\lambda']^T : the transpose of [\lambda']       !!
!!                                                         !!
!!                                                         !!
!!     INPUT: curveA, curveB, ronde                        !!
!!         - curveA, curveB respective meshes of A and B   !!
!!         - kwave wavenumber                              !!
!!         - opt, optional composite variable fixing the   !!
!!                way the double integral is computed      !!
!!                                                         !!
!!     OUTPUT: sngl0                                       !!
!!                                                         !!
!!   See file ecsth.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!   sngl0_cxk : complex wavenumber                        !!
!!   sngl0_rlk : real wavenumber                           !!
!!                                                         !!
!!   generic calls to these functions: sngl0               !! 
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine sngl0_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp) :: kwave
real(wp), dimension(:,:) :: ZR, ZI
!! local variables
integer :: k, l, ks, ls 
real(wp), dimension(2) :: ak, bk, al, bl
type(segment) :: SK, SL
complex(wp) :: Z

! local variables

logical :: Isnear, Helmholtz
! Ns : number of integration nodes for singular integrals
! Nr : number of integration nodes for regular integrals
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)

if( (curveA%Ns .ne. size(ZR,1) ) .or. & 
    (curveA%Ns .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT('Panic: severe error in sngl0')

Helmholtz = abs(kwave) .gt. tiny(1.D0)


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl) 
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
      if (Isnear) then
         Z = intsgl_near(SK,0,SL,0,Ns)
         if(Helmholtz) then 
            GS = GR0_c(SK,SL,kwave)
            Z  = Z + (0.,0.25)*intnum(SK,0,SL,0,GS,Ns)
         end if
      else
         G = G0_c(SK,SL,kwave)
         Z = (0._wp,0.25_wp)*intnum(SK,0,SL,0,G,Nr)
      end if
      ZR(k,l) = real(Z); ZI(k,l) = imag(Z)
   end do
end do

end subroutine sngl0_base_c


subroutine sngl0_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp) :: kwave
real(wp), dimension(:,:) :: ZR, ZI
!! local variables
integer :: k, l, ks, ls 
real(wp), dimension(2) :: ak, bk, al, bl
type(segment) :: SK, SL
complex(wp) :: Z

! local variables
logical :: Isnear, Helmholtz
! Ns : number of integration nodes for singular integrals
! Nr : number of integration nodes for regular integrals
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)


if( (curveA%Ns .ne. size(ZR,1) ) .or. & 
    (curveA%Ns .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in sngl0' )

Helmholtz = abs(kwave) .gt. tiny(1.D0)


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl) 
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
      if (Isnear) then
         Z = intsgl_near(SK,0,SL,0,Ns)
         if(Helmholtz) then 
            GS = GR0_r(SK,SL,kwave)
            Z  = Z + (0._wp,0.25_wp)*intnum(SK,0,SL,0,GS,Ns)
         end if
      else
         G = G0_r(SK,SL,kwave)
         Z = (0._wp,0.25_wp)*intnum(SK,0,SL,0,G,Nr)
      end if
      ZR(k,l) = real(Z); ZI(k,l) = imag(Z)
   end do
end do

end subroutine sngl0_base_r

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function drsngl0 : yields the derivative relatively   !!
!!   to the wavenumber k of sngl0                          !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine drsngl0_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp) :: kwave
real(wp), dimension(:,:) :: ZR, ZI
!! local variables
integer :: k, l, ks, ls 
real(wp), dimension(2) :: ak, bk, al, bl
type(segment) :: SK, SL
complex(wp) :: Z

! local variables

! Ns : number of integration nodes for singular integrals
! Nr : number of integration nodes for regular integrals
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)

if( (curveA%Ns .ne. size(ZR,1) ) .or. & 
    (curveA%Ns .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT('Panic: severe error in sngl0')
    
do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl) 
      G = DRG0_c(SK,SL,kwave)
      Z = (0._wp,0.25_wp)*intnum(SK,0,SL,0,G,Nr)
      ZR(k,l) = real(Z); ZI(k,l) = imag(Z)
   end do
end do

end subroutine drsngl0_base_c

subroutine drsngl0_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp) :: kwave
real(wp), dimension(:,:) :: ZR, ZI
!! local variables
integer :: k, l, ks, ls 
real(wp), dimension(2) :: ak, bk, al, bl
type(segment) :: SK, SL
complex(wp) :: Z

! local variables

! Ns : number of integration nodes for singular integrals
! Nr : number of integration nodes for regular integrals
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)

if( (curveA%Ns .ne. size(ZR,1) ) .or. & 
    (curveA%Ns .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT('Panic: severe error in sngl0')
    
do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl) 
      G = DRG0_r(SK,SL,kwave)
      Z = (0._wp,0.25_wp)*intnum(SK,0,SL,0,G,Nr)
      ZR(k,l) = real(Z); ZI(k,l) = imag(Z)
   end do
end do

end subroutine drsngl0_base_r



!!    END OF drsngl1                                        !! 



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function sngl1 differs from sngl0 in that both        !!
!!   function \lambda and \lambda' are continuous and      !!
!!   linear over each element.                             !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine sngl1_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp) :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: ZE, ZT, PL, PK 
type(segment) :: SK, SL
! local variables
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)


if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in sngl1' )

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)

      if (Isnear) then
         if (Helmholtz) GS = GR0_c(Sk,SL,kwave)
      else
         G = G0_c(Sk,SL,kwave)
      end if
      
      do ke = 1,2
         do le =1,2
            if (Isnear) then
               ZT(ke,le) = intsgl_near(SK,ke-1,SL,le-1, Ns)
               if (Helmholtz) ZT(ke,le) = ZT(ke,le) + & 
                           (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,GS,Ns)
            else
               ZT(ke,le) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,G,Nr)  
            end if
         end do
      end do

      ZE = transpose(PK) .mp. ZT .mp. PL
      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZE(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZE(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZE(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZE(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZE(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZE(2,2))
   end do
end do

end subroutine sngl1_base_c

subroutine sngl1_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp)    :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: ZE, ZT, PL, PK 
type(segment) :: SK, SL
! local variables
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)


if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in sngl1' )

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
       
      if (Isnear) then
         if (Helmholtz) GS = GR0_r(Sk,SL,kwave)
      else
         G = G0_r(Sk,SL,kwave)
      end if
      
      do ke = 1,2
         do le =1,2
            if (Isnear) then
               ZT(ke,le) = intsgl_near(SK,ke-1,SL,le-1, Ns)
               if (Helmholtz) then 
                  ZT(ke,le) = ZT(ke,le) + & 
                           (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,GS,Ns)
               end if
            else
               ZT(ke,le) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,G,Nr)
            end if
         end do
      end do

      ZE = transpose(PK) .mp. ZT .mp. PL
      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZE(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZE(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZE(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZE(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZE(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZE(2,2))
   end do
end do

end subroutine sngl1_base_r


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function sngl10 differs from sngl0 in that            !!
!!   function  \lambda' is continuous and                  !!
!!   linear over each element.                             !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine sngl10_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp) :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp) :: ZE(2,1), ZT(2,1), PK(2,2) 
type(segment) :: SK, SL
! local variables
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)


if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in sngl10' )

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); 
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)

      if (Isnear) then
         if (Helmholtz) GS = GR0_c(Sk,SL,kwave)
      else
         G = G0_c(Sk,SL,kwave)
      end if
      
      do ke = 1,2
            if (Isnear) then
               ZT(ke,1) = intsgl_near(SK,ke-1,SL,0, Ns)
               if (Helmholtz) ZT(ke,1) = ZT(ke,1) + & 
                           (0._wp,0.25_wp)*intnum(SK,ke-1,SL,0,GS,Ns)
            else
               ZT(ke,1) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,0,G,Nr)  
            end if
      end do

      ZE = transpose(PK) .mp. ZT 
      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZE(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZE(2,1))
   end do
end do

end subroutine sngl10_base_c


subroutine sngl10_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp) :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp) :: ZE(2,1), ZT(2,1), PK(2,2) 
type(segment) :: SK, SL
! local variables
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)


if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in sngl10' )

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); 
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)

      if (Isnear) then
         if (Helmholtz) GS = GR0_r(Sk,SL,kwave)
      else
         G = G0_r(Sk,SL,kwave)
      end if
      
      do ke = 1,2
            if (Isnear) then
               ZT(ke,1) = intsgl_near(SK,ke-1,SL,0, Ns)
               if (Helmholtz) ZT(ke,1) = ZT(ke,1) + & 
                           (0._wp,0.25_wp)*intnum(SK,ke-1,SL,0,GS,Ns)
            else
               ZT(ke,1) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,0,G,Nr)  
            end if
      end do

      ZE = transpose(PK) .mp. ZT 
      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZE(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZE(2,1))
   end do
end do

end subroutine sngl10_base_r

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function dndble returns the matrix associated to the  !!
!!   bilinear form associated to the normal derivative on  !!
!!   a curve A of a double-layer potential created by a    !!
!!   density J  on a curve A                               !!
!!                                                         !!
!!   - \int_A \int_B \partial_{n_y} G(x,y)                 !!
!!             J(y) J'(x) dB(y) dA(x) =                    !!
!!                                                         !!
!!             = [J']^T [dndble] [J]                       !!
!!                                                         !!
!!     A and B : two curves meshed in segments             !!
!!        J : generic continuous function linear over each !!
!!                  segment of B                           !!
!!        J': generic continuous function linear over each !!
!!                  segment of B                           !!
!!                                                         !!
!!     When A (resp. B) is not a closed curve, J (resp. J')!!
!!     vanishes at the end points of A (resp. B)           !!
!!                                                         !!
!!              [J]  : column-vector that components are   !!
!!     the values of J on internal nodes of B              !!
!!            [J']^T : the transpose of [J']               !!
!!                                                         !!
!!     INPUT: curveA, curveB, ronde                        !!
!!         - curveA, curveB respective meshes of A and B   !!
!!         - ronde wavenumber                              !!
!!                                                         !!
!!     OUTPUT: dndble                                      !!
!!                                                         !!
!!   See file rcste.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!            

subroutine dndble_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp)    :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: ZS, ZD, PL, PK 
type(segment) :: SK, SL
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)
real(wp)    :: ps

if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT('Panic: severe error in dndble')

ZR = 0.; ZI = 0.;  !! hum? value not modified by the subroutine ??
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
       
      if (Isnear) then
         ZS = intsgl_near(SK,0,SL,0,Ns)
         if (Helmholtz) then
            GS = GR0_r(Sk,SL,kwave)
            ZS = ZS + (0._wp,0.25_wp)*intnum(SK,0,SL,0,GS,Ns)
            ps = -k*k*pscalair(SK%t,SL%t)
         end if
      else
         G = G0_r(Sk,SL,kwave)
         ZS = (0._wp,0.25_wp)*intnum(SK,0,SL,0,G,Nr)
         if (Helmholtz) ps = -pscalair(SK%t,SL%t)
      end if
      ZD(1,1) = ZS(1,1)
      ZS = ZS/(SK%l*SL%l)
      ZS(1,2) = -ZS(1,2); ZS(2,1) = -ZS(2,1)

      if (Helmholtz) then
         ps = - pscalair(SK%t,SL%t)
         do ke = 1,2
            do le =1,2
               if ( (ke+le-2) .eq. 0 ) cycle
               if (Isnear) then
                  ZD(ke,le) = intsgl_near(SK,ke-1,SL,le-1, Ns)
                  ZD(ke,le) = ZD(ke,le) + & 
                       (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,GS,Ns)
               else
                  ZD(ke,le) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,G,Nr)
               end if
            end do
         end do
      end if

      if (Helmholtz) ZS = ZS + ps*kwave**2*(transpose(PK) .mp. ZD .mp. PL)

      ZR(k ,l )  = ZR(k ,l ) + real(ZS(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZS(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZS(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZS(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZS(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZS(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZS(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZS(2,2))
   end do
end do

end subroutine dndble_base_r



subroutine dndble_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp) :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: ZS, ZD, PL, PK 
type(segment) :: SK, SL
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)
real(wp)    :: ps

if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in dndble')

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
       
      if (Isnear) then
         ZS = intsgl_near(SK,0,SL,0,Ns)
         if (Helmholtz) then
            GS = GR0_c(Sk,SL,kwave)
            ZS = ZS + (0._wp,0.25_wp)*intnum(SK,0,SL,0,GS,Ns)
            ps = -pscalair(SK%t,SL%t)
         end if
      else
         G = G0_c(Sk,SL,kwave)
         ZS = (0._wp,0.25_wp)*intnum(SK,0,SL,0,G,Nr)
         if (Helmholtz) ps = -k*k*pscalair(SK%t,SL%t)
      end if
      ZD(1,1) = ZS(1,1)
      ZS = ZS/(SK%l*SL%l)
      ZS(1,2) = -ZS(1,2); ZS(2,1) = -ZS(2,1)

      if (Helmholtz) then
         ps = - pscalair(SK%t,SL%t)
         do ke = 1,2
            do le =1,2
               if ( (ke+le-2) .eq. 0 ) cycle
               if (Isnear) then
                  ZD(ke,le) = intsgl_near(SK,ke-1,SL,le-1, Ns)
                  ZD(ke,le) = ZD(ke,le) + & 
                       (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,GS,Ns)
               else
                  ZD(ke,le) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,G,Nr)
               end if
            end do
         end do
      end if

      if (Helmholtz) ZS = ZS + ps*kwave**2*(transpose(PK) .mp. ZD .mp. PL)

      ZR(k ,l )  = ZR(k ,l ) + real(ZS(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZS(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZS(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZS(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZS(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZS(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZS(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZS(2,2))
   end do
end do

end subroutine dndble_base_c

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function sngltt1 returns the matrix associated to the !!
!!   bilinear form                                         !!
!!                                                         !!
!!   - \int_A \int_B \partial_{n_y} G(x,y)                 !!
!!             J(y)\tau_y J'(x)\tau_x dB(y) dA(x) =        !!
!!                                                         !!
!!             = [J']^T [dndble] [J]                       !!
!!                                                         !!
!!     A and B : two curves meshed in segments             !!
!!        J : generic continuous function linear over each !!
!!                  segment of B                           !!
!!        J': generic continuous function linear over each !!
!!                  segment of B                           !!
!!                                                         !!
!!     When A (resp. B) is not a closed curve, J (resp. J')!!
!!     vanishes at the end points of A (resp. B)           !!
!!                                                         !!
!!              [J]  : column-vector that components are   !!
!!     the values of J on internal nodes of B              !!
!!            [J']^T : the transpose of [J']               !!
!!                                                         !!
!!     INPUT: curveA, curveB, ronde                        !!
!!         - curveA, curveB respective meshes of A and B   !!
!!         - ronde wavenumber                              !!
!!                                                         !!
!!     OUTPUT: dndble                                      !!
!!                                                         !!
!!   See file rcste.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!            

subroutine sngltt1_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp)    :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: ZD, PL, PK 
type(segment) :: SK, SL
logical :: Isnear
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)
real(wp)    :: ps

if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT('Panic: severe error in sngltt1')

ZR = 0.; ZI = 0; 

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
      
      if (Isnear) then
         GS = GR0_r(Sk,SL,kwave)
      else
         G = G0_r(Sk,SL,kwave)
      end if
       
      ps = pscalair(SK%t,SL%t)
         do ke = 1,2
            do le =1,2
               if (Isnear) then
                  ZD(ke,le) = intsgl_near(SK,ke-1,SL,le-1, Ns)
                  ZD(ke,le) = ZD(ke,le) + & 
                       (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,GS,Ns)
               else
                  ZD(ke,le) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,G,Nr)
               end if
            end do
         end do

      ZD = ps*(transpose(PK) .mp. ZD .mp. PL)

      ZR(k ,l )  = ZR(k ,l ) + real(ZD(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZD(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZD(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZD(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZD(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZD(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZD(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZD(2,2))
   end do
end do

end subroutine sngltt1_base_r

subroutine sngltt1_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp)    :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: ZD, PL, PK 
type(segment) :: SK, SL
logical :: Isnear
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)
real(wp)    :: ps

if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT('Panic: severe error in sngltt1')

ZR = 0.; ZI = 0; 

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)

      if (Isnear) then
         GS = GR0_c(Sk,SL,kwave)
      else
         G = G0_c(Sk,SL,kwave)
      end if
             
      ps = pscalair(SK%t,SL%t)
         do ke = 1,2
            do le =1,2
               if (Isnear) then
                  ZD(ke,le) = intsgl_near(SK,ke-1,SL,le-1, Ns)
                  ZD(ke,le) = ZD(ke,le) + & 
                       (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,GS,Ns)
               else
                  ZD(ke,le) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,G,Nr)
               end if
            end do
         end do

      ZD = ps*(transpose(PK) .mp. ZD .mp. PL)

      ZR(k ,l )  = ZR(k ,l ) + real(ZD(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZD(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZD(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZD(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZD(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZD(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZD(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZD(2,2))
   end do
end do

end subroutine sngltt1_base_c


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function snglnt1 returns the matrix associated to the !!
!!   bilinear form                                         !!
!!                                                         !!
!!   - \int_A \int_B \partial_{n_y} G(x,y)                 !!
!!             J(y)\tau_y J'(x)n_x dB(y) dA(x) =        !!
!!                                                         !!
!!             = [J']^T [dndble] [J]                       !!
!!                                                         !!
!!     A and B : two curves meshed in segments             !!
!!        J : generic continuous function linear over each !!
!!                  segment of B                           !!
!!        J': generic continuous function linear over each !!
!!                  segment of B                           !!
!!                                                         !!
!!     When A (resp. B) is not a closed curve, J (resp. J')!!
!!     vanishes at the end points of A (resp. B)           !!
!!                                                         !!
!!              [J]  : column-vector that components are   !!
!!     the values of J on internal nodes of B              !!
!!            [J']^T : the transpose of [J']               !!
!!                                                         !!
!!     INPUT: curveA, curveB, ronde                        !!
!!         - curveA, curveB respective meshes of A and B   !!
!!         - ronde wavenumber                              !!
!!                                                         !!
!!     OUTPUT: dndble                                      !!
!!                                                         !!
!!   See file rcste.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!            

subroutine snglnt1_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp)    :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl, nk
complex(wp), dimension(2,2) :: ZD, PL, PK 
type(segment) :: SK, SL
logical :: Isnear
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)
real(wp)    :: ps

if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT('Panic: severe error in snglnt1')

ZR = 0.; ZI = 0; 

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
      if (Isnear) then
         GS = GR0_r(Sk,SL,kwave)
      else
         G = G0_r(Sk,SL,kwave)
      end if      
      nk(1) = - SK%t(2)
      nk(2) =   SK%t(1) 
      ps = pscalair(nk,SL%t)
         do ke = 1,2
            do le =1,2
               if (Isnear) then
                  ZD(ke,le) = intsgl_near(SK,ke-1,SL,le-1, Ns)
                  ZD(ke,le) = ZD(ke,le) + & 
                       (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,GS,Ns)
               else
                  ZD(ke,le) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,G,Nr)
               end if
            end do
         end do

      ZD = ps*(transpose(PK) .mp. ZD .mp. PL)

      ZR(k ,l )  = ZR(k ,l ) + real(ZD(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZD(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZD(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZD(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZD(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZD(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZD(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZD(2,2))
   end do
end do

end subroutine snglnt1_base_r

subroutine snglnt1_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp)    :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl, nk
complex(wp), dimension(2,2) :: ZD, PL, PK 
type(segment) :: SK, SL
logical :: Isnear
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)
real(wp)    :: ps

if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT('Panic: severe error in snglnt1')

ZR = 0.; ZI = 0; 

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
      if (Isnear) then
         GS = GR0_c(Sk,SL,kwave)
      else
         G = G0_c(Sk,SL,kwave)
      end if      
      nk(1) = - SK%t(2)
      nk(2) =   SK%t(1) 
      ps = pscalair(nk,SL%t)
         do ke = 1,2
            do le =1,2
               if (Isnear) then
                  ZD(ke,le) = intsgl_near(SK,ke-1,SL,le-1, Ns)
                  ZD(ke,le) = ZD(ke,le) + & 
                       (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,GS,Ns)
               else
                  ZD(ke,le) = (0._wp,0.25_wp)*intnum(SK,ke-1,SL,le-1,G,Nr)
               end if
            end do
         end do

      ZD = ps*(transpose(PK) .mp. ZD .mp. PL)

      ZR(k ,l )  = ZR(k ,l ) + real(ZD(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZD(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZD(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZD(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZD(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZD(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZD(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZD(2,2))
   end do
end do

end subroutine snglnt1_base_c

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function dble0 returns the matrix associated to the   !!
!!   bilinear form associated to the value of a double     !!
!!   layer potential created by a density J on a           !!
!!   curve A on a curve B                                  !!
!!                                                         !!
!!   - \int_A \int_B \partial_{n(y)} G(x,y)                !!
!!                                  J(y) J'(x) dB(y) dA(x) !!
!!                                                         !!
!!                        = [J']^T [dble0] [J]             !!
!!                                                         !!
!!   BE AWARE ON THE MINUS SIGN AND THAT THE PARTIAL       !!
!!   DERIVATIVE IS TAKEN RELATIVELY TO VARIABLE y          !!
!!                                                         !!
!!     A and B : two curves meshed in segments             !!
!!        J  : generic function constant over each         !!
!!                  segment of B                           !!
!!        J' : generic function constant over each         !!
!!                  segment of A                           !!
!!                                                         !!
!!        [J]  : column-vector that components are         !!
!!     the values of J on each segment                     !!
!!        [J']^T : the transpose of [J']                   !!
!!                                                         !!
!!     The postfix 00 refers to functions P_0 both on      !!
!!     segments of B and of A                              !!
!!                                                         !!
!!     INPUT: curveA, curveB, ronde                        !!
!!         - curveA, curveB respective meshes of A and B   !!
!!         - ronde wavenumber                              !!
!!                                                         !!
!!     OUTPUT: dble00                                      !!
!!                                                         !!
!!   See file rcsth.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!            

subroutine dble0_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp) :: kwave
real(wp), dimension(:,:) :: ZR, ZI
!! local variables
integer :: k, l, ks, ls 
real(wp), dimension(2) :: ak, bk, al, bl
type(segment) :: SK, SL
complex(wp) :: Z

! local variables
real(wp)    :: d1, d2, dmin
logical :: Isnear, Helmholtz
! Ns : number of integration nodes for singular integrals
! Nr : number of integration nodes for regular integrals
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)


if( (curveA%Ns .ne. size(ZR,1) ) .or. & 
    (curveA%Ns .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in sngl0')

Helmholtz = abs(kwave) .gt. tiny(1.D0)


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl)
      d1 = rlong(ak,al)+rlong(bk,bl)
      d2 = rlong(ak,bl)+rlong(bk,al)
      dmin = min(d1,d2)
      if (dmin .le. epsilon(dmin)*(max(SK%l,SL%l))) then
         Z = 0
      else
         Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
         if (Isnear) then
            Z = intdbl_near(SK,0,SL,0,Ns)
            if(Helmholtz) then 
               GS = GR1_r(SK,SL,kwave)
               Z  = Z + (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,GS,Ns)
            end if
         else
            G = G1_r(SK,SL,kwave)
            if(Helmholtz) then
               Z = (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,G,Nr)
            else
               Z = 0.5_wp*intnum(SK,0,SL,0,G,Nr)/pi
            end if
         end if
      end if
      ZR(k,l) = real(Z); ZI(k,l) = imag(Z)
   end do
end do

end subroutine dble0_base_r

subroutine dble0_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp) :: kwave
real(wp), dimension(:,:) :: ZR, ZI
!! local variables
integer :: k, l, ks, ls 
real(wp), dimension(2) :: ak, bk, al, bl
type(segment) :: SK, SL
complex(wp) :: Z

! local variables
real(wp)    :: d1, d2, dmin
logical :: Isnear, Helmholtz
! Ns : number of integration nodes for singular integrals
! Nr : number of integration nodes for regular integrals
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)

if( (curveA%Ns .ne. size(ZR,1) ) .or. & 
    (curveA%Ns .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in sngl0')

Helmholtz = abs(kwave) .gt. tiny(1.D0)


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl)
      d1 = rlong(ak,al)+rlong(bk,bl)
      d2 = rlong(ak,bl)+rlong(bk,al)
      dmin = min(d1,d2)
      if (dmin .le. epsilon(dmin)*(max(SK%l,SL%l))) then
         Z = 0
      else
         Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
         if (Isnear) then
            Z = intdbl_near(SK,0,SL,0,Ns)
            if(Helmholtz) then 
               GS = GR1_c(SK,SL,kwave)
               Z  = Z + (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,GS,Ns)
            end if
         else
            G = G1_c(SK,SL,kwave)
            if(Helmholtz) then
               Z = (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,G,Nr)
            else
               Z = 0.5_wp*intnum(SK,0,SL,0,G,Nr)/pi
            end if
         end if
      end if
      ZR(k,l) = real(Z); ZI(k,l) = imag(Z)
   end do
end do

end subroutine dble0_base_c


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function dble01 returns the matrix associated to the  !!
!!   bilinear form associated to the value of a double     !!
!!   layer potential created by a density J on a           !!
!!   curve A on a curve B                                  !!
!!                                                         !!
!!   - \int_A \int_B \partial_{n(y)} G(x,y)                !!
!!                                  J(y) J'(x) dB(y) dA(x) !!
!!                                                         !!
!!                        = [J']^T [dble01] [J]            !!
!!                                                         !!
!!   The postfix 01 refers to a function J' P_0 by element !!
!!   and a function J P_1 by element                       !!
!!                                                         !!
!!   This function does the same work than dble00 except   !!
!!   that the function J is continuous and linear by       !!
!!   element. Note that when B is an open curve J vanishes !!
!!   at the terminating nodes.                             !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


subroutine dble01_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp)    :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: PL 
complex(wp) :: ZD(1,2), ZE(1,2)
type(segment) :: SK, SL
! local variables
real(wp)    :: d1, d2, dmin
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr) 
integer     :: NA, NB

NA = size(curveA%Coord,2)
NB = size(curveB%Coord,2)

if( (curveA%Ns .ne. size(ZR,1) ) .or. & 
    (curveA%Ns .ne. size(ZI,1) ) .or. &
    (NB .ne. size(ZR,2) ) .or. & 
    (NB .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in dble01')

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); 
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      d1 = rlong(ak,al)+rlong(bk,bl)
      d2 = rlong(ak,bl)+rlong(bk,al)
      dmin = min(d1,d2)
      if (dmin .le. epsilon(dmin)*(max(SK%l,SL%l))) then
         ZD = 0._wp  
      else
         Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
         if (Isnear) then
            if (Helmholtz) GS = GR1_r(Sk,SL,kwave)
         else
            G = G1_r(Sk,SL,kwave)
         end if
         
         if (Isnear) then
            ZD(1,1) = intdbl_near(SK,0,SL,0,Ns)
            ZD(1,2) = intdbl_near(SK,0,SL,1,Ns)
            if(Helmholtz) then 
               ZD(1,1) = ZD(1,1) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,GS,Ns)
               ZD(1,2) = ZD(1,2) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,1,GS,Ns)
            end if
         else
            if (Helmholtz) then
               ZD(1,1) = (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,G,Nr)
               ZD(1,2) = (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,1,G,Nr)
            else
               ZD(1,1) = 0.5_wp*intnum(SK,0,SL,0,G,Nr)/pi
               ZD(1,2) = 0.5_wp*intnum(SK,0,SL,1,G,Nr)
            end if
         end if
      end if
      ZE = ZD .mp. PL
      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZE(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZE(1,2))

   end do
end do

end subroutine dble01_base_r

subroutine dble01_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp) :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: PL 
complex(wp) :: ZD(1,2), ZE(1,2)
type(segment) :: SK, SL
! local variables
real(wp)    :: d1, d2, dmin
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)
integer :: NA, NB

NA = size(curveA%Coord,2)
NB = size(curveB%Coord,2)

if( (curveA%Ns .ne. size(ZR,1) ) .or. & 
    (curveA%Ns .ne. size(ZI,1) ) .or. &
    (NB .ne. size(ZR,2) ) .or. & 
    (NB .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in dble01')

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); 
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      d1 = rlong(ak,al)+rlong(bk,bl)
      d2 = rlong(ak,bl)+rlong(bk,al)
      dmin = min(d1,d2)
      if (dmin .le. epsilon(dmin)*(max(SK%l,SL%l))) then
         ZD = 0._wp  
      else
         Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
         if (Isnear) then
            if (Helmholtz) GS = GR1_c(Sk,SL,kwave)
         else
            G = G1_c(Sk,SL,kwave)
         end if
         
         if (Isnear) then
            ZD(1,1) = intdbl_near(SK,0,SL,0,Ns)
            ZD(1,2) = intdbl_near(SK,0,SL,1,Ns)
            if(Helmholtz) then 
               ZD(1,1) = ZD(1,1) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,GS,Ns)
               ZD(1,2) = ZD(1,2) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,1,GS,Ns)
            end if
         else
            if (Helmholtz) then
               ZD(1,1) = (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,G,Nr)
               ZD(1,2) = (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,1,G,Nr)
            else
               ZD(1,1) = 0.5_wp*intnum(SK,0,SL,0,G,Nr)/pi
               ZD(1,2) = 0.5_wp*intnum(SK,0,SL,1,G,Nr)
            end if
         end if
      end if
      ZE = ZD .mp. PL
      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZE(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZE(1,2))

   end do
end do

end subroutine dble01_base_c


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function dble10 returns the matrix associated to the  !!
!!   bilinear form associated to the value of a double     !!
!!   layer potential created by a density J on a           !!
!!   curve A on a curve B                                  !!
!!                                                         !!
!!   - \int_A \int_B \partial_{n(y)} G(x,y)                !!
!!                                  J(y) J'(x) dB(y) dA(x) !!
!!                                                         !!
!!                        = [J']^T [dble01] [J]            !!
!!                                                         !!
!!   The postfix 10 refers to a function J P_0 by element  !!
!!   and a function J' P_1 by element (see dble01)         !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


subroutine dble10_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp)    :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: PK 
complex(wp) :: ZD(2,1), ZE(2,1)
type(segment) :: SK, SL
! local variables
real(wp)    :: d1, d2, dmin
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr) 
integer     :: NA, NB

NA = size(curveA%Coord,2)
NB = size(curveB%Coord,2)

if( (NA .ne. size(ZR,1) ) .or. & 
    (NA .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in dble10')

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); 
      d1 = rlong(ak,al)+rlong(bk,bl)
      d2 = rlong(ak,bl)+rlong(bk,al)
      dmin = min(d1,d2)
      if (dmin .le. epsilon(dmin)*(max(SK%l,SL%l))) then
         ZD = 0._wp  
      else
         Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
         if (Isnear) then
            if (Helmholtz) GS = GR1_r(Sk,SL,kwave)
         else
            G = G1_r(Sk,SL,kwave)
         end if
         
         if (Isnear) then
            ZD(1,1) = intdbl_near(SK,0,SL,0,Ns)
            ZD(2,1) = intdbl_near(SK,1,SL,0,Ns)
            if(Helmholtz) then 
               ZD(1,1) = ZD(1,1) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,GS,Ns)
               ZD(2,1) = ZD(2,1) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,1,SL,0,GS,Ns)
            end if
         else
            if (Helmholtz) then
               ZD(1,1) = (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,G,Nr)
               ZD(2,1) = (0._wp,0.25_wp)*kwave*intnum(SK,1,SL,0,G,Nr)
            else
               ZD(1,1) = 0.5_wp*intnum(SK,0,SL,0,G,Nr)/pi
               ZD(2,1) = 0.5_wp*intnum(SK,1,SL,0,G,Nr)/pi
            end if
         end if
      end if

      ZE = transpose(PK) .mp. ZD
      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZE(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZE(2,1))

   end do
end do

end subroutine dble10_base_r

subroutine dble10_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp) :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: PK 
complex(wp) :: ZD(2,1), ZE(2,1)
type(segment) :: SK, SL
! local variables
real(wp)    :: d1, d2, dmin
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr) 
integer     :: NA, NB

NA = size(curveA%Coord,2)
NB = size(curveB%Coord,2)

if( (NA .ne. size(ZR,1) ) .or. & 
    (NA .ne. size(ZI,1) ) .or. &
    (curveB%Ns .ne. size(ZR,2) ) .or. & 
    (curveB%Ns .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in dble10')

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); 
      d1 = rlong(ak,al)+rlong(bk,bl)
      d2 = rlong(ak,bl)+rlong(bk,al)
      dmin = min(d1,d2)
      if (dmin .le. epsilon(dmin)*(max(SK%l,SL%l))) then
         ZD = 0._wp  
      else
         Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
         if (Isnear) then
            if (Helmholtz) GS = GR1_c(Sk,SL,kwave)
         else
            G = G1_c(Sk,SL,kwave)
         end if
         
         if (Isnear) then
            ZD(1,1) = intdbl_near(SK,0,SL,0,Ns)
            ZD(2,1) = intdbl_near(SK,1,SL,0,Ns)
            if(Helmholtz) then 
               ZD(1,1) = ZD(1,1) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,GS,Ns)
               ZD(2,1) = ZD(2,1) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,1,SL,0,GS,Ns)
            end if
         else
            if (Helmholtz) then
               ZD(1,1) = (0._wp,0.25_wp)*kwave*intnum(SK,0,SL,0,G,Nr)
               ZD(2,1) = (0._wp,0.25_wp)*kwave*intnum(SK,1,SL,0,G,Nr)
            else
               ZD(1,1) = 0.5_wp*intnum(SK,0,SL,0,G,Nr)/pi
               ZD(2,1) = 0.5_wp*intnum(SK,1,SL,0,G,Nr)/pi
            end if
         end if
      end if

      ZE = transpose(PK) .mp. ZD
      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZE(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZE(2,1))

   end do
end do

end subroutine dble10_base_c


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function dble11 returns the matrix associated to the  !!
!!   bilinear form associated to the value of a double     !!
!!   layer potential created by a density J on a           !!
!!   curve A on a curve B                                  !!
!!                                                         !!
!!   - \int_A \int_B \partial_{n(y)} G(x,y)                !!
!!                                  J(y) J'(x) dB(y) dA(x) !!
!!                                                         !!
!!                        = [J']^T [dble00] [J]            !!
!!                                                         !!
!!   BE AWARE ON THE MINUS SIGN AND THAT THE PARTIAL       !!
!!   DERIVATIVE IS TAKEN RELATIVELY TO VARIABLE y          !!
!!                                                         !!
!!     A and B : two curves meshed in segments             !!
!!     J  : generic continuous function linear over each   !!
!!                  segment of B                           !!
!!     J' : generic continuous function linear over each   !!
!!                  segment of A                           !!
!!                                                         !!
!!        [J]  : column-vector that components are         !!
!!     the values of J on each internal node               !!
!!        [J']^T : the transpose of [J']                   !!
!!                                                         !!
!!     The postfix 11 refers to functions P_1 both on      !!
!!     segments of B and of A                              !!
!!                                                         !!
!!     INPUT: curveA, curveB, ronde                        !!
!!         - curveA, curveB respective meshes of A and B   !!
!!         - ronde wavenumber                              !!
!!                                                         !!
!!     OUTPUT: dble11                                      !!
!!                                                         !!
!!   See file rcste.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!            

subroutine dble1_base_r(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
real(wp)    :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: ZE, ZT, PL, PK 
type(segment) :: SK, SL
! local variables
real(wp)    :: d1, d2, dmin
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)


if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in dble1')

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      d1 = rlong(ak,al)+rlong(bk,bl)
      d2 = rlong(ak,bl)+rlong(bk,al)
      dmin = min(d1,d2)
      if (dmin .le. epsilon(dmin)*(max(SK%l,SL%l))) then
         ZE = 0
      else
         Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
         if (Isnear) then
            if (Helmholtz) GS = GR1_r(Sk,SL,kwave)
         else
            G = G1_r(Sk,SL,kwave)
         end if
      
         do ke = 1,2
            do le =1,2
               if (Isnear) then
                  ZT(ke,le) = intdbl_near(SK,ke-1,SL,le-1, Ns)
                  if (Helmholtz) then 
                     ZT(ke,le) = ZT(ke,le) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,ke-1,SL,le-1,GS,Ns)
                  end if
               else
                  if (Helmholtz) then
                     ZT(ke,le) = (0._wp,0.25_wp)*kwave*intnum(SK,ke-1,SL,le-1,G,Nr)
                  else
                     ZT(ke,le) = 0.5_wp*intnum(SK,ke-1,SL,le-1,G,Nr)/pi
                  end if
               end if
            end do
         end do


      ZE = transpose(PK) .mp. ZT .mp. PL
   end if

      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZE(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZE(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZE(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZE(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZE(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZE(2,2))
   end do
end do

end subroutine dble1_base_r

subroutine dble1_base_c(curveA, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveA, curveB
complex(wp) :: kwave
real(wp),  dimension(:,:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp), dimension(2,2) :: ZE, ZT, PL, PK 
type(segment) :: SK, SL
! local variables
real(wp)    :: d1, d2, dmin
logical :: Isnear, Helmholtz
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns,Ns), G(Nr,Nr)


if( (size(curveA%Coord,2) .ne. size(ZR,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(ZI,1) ) .or. &
    (size(curveB%Coord,2) .ne. size(ZR,2) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in dble1')

ZR = 0.; ZI = 0; 
Helmholtz = abs(kwave) .gt. tiny(1.D0) ! initialization


do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); PK = p1tond(SK)
   do l=1,curveB%Ns
      ls = l + 1
      if(ls .gt. size(curveB%Coord,2)) ls = 1
      al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
      SL = ptoseg(al,bl); PL = p1tond(SL)
      d1 = rlong(ak,al)+rlong(bk,bl)
      d2 = rlong(ak,bl)+rlong(bk,al)
      dmin = min(d1,d2)
      if (dmin .le. epsilon(dmin)*(max(SK%l,SL%l))) then
         ZE = 0
      else
         Isnear = rlong(SK%m,SL%m) .le. D*(SK%l+SL%l)
         if (Isnear) then
            if (Helmholtz) GS = GR1_c(Sk,SL,kwave)
         else
            G = G1_c(Sk,SL,kwave)
         end if
      
         do ke = 1,2
            do le =1,2
               if (Isnear) then
                  ZT(ke,le) = intdbl_near(SK,ke-1,SL,le-1, Ns)
                  if (Helmholtz) then 
                     ZT(ke,le) = ZT(ke,le) + & 
                          (0._wp,0.25_wp)*kwave*intnum(SK,ke-1,SL,le-1,GS,Ns)
                  end if
               else
                  if (Helmholtz) then
                     ZT(ke,le) = (0._wp,0.25_wp)*kwave*intnum(SK,ke-1,SL,le-1,G,Nr)
                  else
                     ZT(ke,le) = 0.5_wp*intnum(SK,ke-1,SL,le-1,G,Nr)/pi
                  end if
               end if
            end do
         end do

      ZE = transpose(PK) .mp. ZT .mp. PL
   end if

      ZR(k ,l )  = ZR(k ,l ) + real(ZE(1,1))
      ZI(k ,l )  = ZI(k ,l ) + imag(ZE(1,1))
      ZR(ks,l )  = ZR(ks,l ) + real(ZE(2,1))
      ZI(ks,l )  = ZI(ks,l ) + imag(ZE(2,1))
      ZR(k ,ls)  = ZR(k ,ls) + real(ZE(1,2))
      ZI(k ,ls)  = ZI(k ,ls) + imag(ZE(1,2))
      ZR(ks,ls)  = ZR(ks,ls) + real(ZE(2,2))
      ZI(ks,ls)  = ZI(ks,ls) + imag(ZE(2,2))
   end do
end do

end subroutine dble1_base_c


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Function mass0  returns the mass matrix associated    !!
!!   bilinear form                                         !!
!!                                                         !!
!!   \int_A  J(y) J'(x) dB(y) dA(x)                        !!
!!                                                         !!
!!                        = [J']^T [mass00] [J]            !!
!!                                                         !!
!!   See function sngl0  or dble00 for the relation        !!
!!   linking J and J' to [J] and [J'] respectively         !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine mass0_base(curveA, Z)
implicit none
type(curve) :: curveA
real(wp), dimension(:,:) :: Z
!! local variables
integer :: k, ks
real(wp), dimension(2) :: ak, bk

if( (curveA%Ns .ne. size(Z,1) ) .or. & 
    (curveA%Ns .ne. size(Z,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in mass0')

Z = 0._wp
do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   Z(k,k) = rlong(ak,bk)
end do

end subroutine mass0_base

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!!   Functions massdiag and mass1 return the mass matrix   !!
!!   associated the bilinear form                          !!
!!                                                         !!
!!   \int_A  J(y) J'(x) dB(y) dA(x)                        !!
!!                                                         !!
!!                        = [J']^T [mass(1)(diag)] [J]     !!
!!                                                         !!
!!   mass1    : returns the exact matrix                   !!
!!   massdiag : returns a diagonal matrix obtained         !!
!!              a lumping process                          !!
!!                                                         !!
!!   See function sngl1  or dble11 for the relation        !!
!!   linking J and J' to [J] and [J'] respectively         !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine massdiag(curveA, Z) 
implicit none
type(curve) :: curveA
real(wp), dimension(:,:) :: Z

!! local variables
integer :: k, ks
real(wp), dimension(2) :: ak, bk
type(segment) SK


if( (size(curveA%Coord,2) .ne. size(Z,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(Z,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in mass')

Z = 0._wp
do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   Z(k ,k ) = Z(k ,k ) + SK%l*0.5
   Z(ks,ks) = Z(ks,ks) + SK%l*0.5
end do

end subroutine massdiag

subroutine mass1_base(curveA, Z) 
implicit none
type(curve) :: curveA
real(wp), dimension(:,:) :: Z

!! local variables
integer :: k, ks
real(wp), dimension(2) :: ak, bk
type(segment) SK

if( (size(curveA%Coord,2) .ne. size(Z,1) ) .or. & 
    (size(curveA%Coord,2) .ne. size(Z,2) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in mass')

Z = 0._wp
do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)  
   Z(k ,k ) = Z(k ,k ) + SK%l/3._wp
   Z(ks,ks) = Z(ks,ks) + SK%l/3._wp
   Z(k ,ks) = Z(k ,ks) + SK%l/6._wp
   Z(ks,k ) = Z(ks,k ) + SK%l/6._wp
end do

end subroutine mass1_base

subroutine ds2p1_base(Z, curveA)
implicit none
type(curve) :: curveA
real(wp), &
 & dimension(size(curveA%Coord,2),size(curveA%Coord,2)) :: Z
!! local variables
integer :: k, ks
real(wp), dimension(2) :: ak, bk
real(wp) :: S
type(segment) SK

Z = 0._wp
do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk); S = 1._wp/SK%l  
   Z(k ,k ) = Z(k ,k ) + S
   Z(ks,ks) = Z(ks,ks) + S
   Z(k ,ks) = Z(k ,ks) - S
   Z(ks,k ) = Z(ks,k ) - S
end do

end subroutine ds2p1_base


!************************************************************
!                                                           *
!                                                           *
!    P A R T  O F  T H E  C O D E  R E L A T I V E   T O    *
!           T H E  C O M P U T A T I O N  O F               *
!                     T H E  F E E D I N G                  *
!          O F  A  R A D I A T I N G   S Y S T E M          *
!             O R  T H E  I L L U M I N A T I O N           *
!                 O F  A  S C A T T E R E R                 *
!                                                           *
!************************************************************


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function planeTHE0 returns the vector associated to the !!
!! linear form describing the opposite of the power        !!
!! resulting from testing the electric field relative to   !!
!! a plane wave with a complex amplitude equal to 1 for    !!
!! a TH polarization (field H is in the plan of            !!
!! propagation) by a test elecric current J' being         !!
!! constant over each segment                              !! 
!!                                                         !!
!! -\int_A  exp(-ik Nu.x) J'(x) dA(x) = [J']^T [planeTHE0] !!
!!                                                         !!
!!                                                         !!
!!     A : a curve meshed in segments                      !!
!!     J': generic function constant over each             !!
!!                  segment of A                           !!
!!     Nu = (cos(theta_0),sin(theta_0)) : (opposite)       !!
!!       direction of propagation of the incident wave     !!
!!                                                         !!
!!        [J']  : column-vector that components are        !!
!!     the values of J' on each segment                    !!
!!                                                         !!
!!     The postfix 0 refers to functions P_0 on segments   !!
!!                                                         !!
!!     INPUT:                                              !!
!!         - curveA,  mesh of A                            !!
!!         - ronde, wavenumber                             !!
!!         - theta_0, angle in radians giving the          !! 
!!           (opposite) direction of propagation           !!
!!           of the incident wave                          !!
!!         - Np (optional), number of Gauss points for     !!
!!           numerical integration on each segment         !!
!!                                                         !!
!!     OUTPUT: planeTHE0                                   !!
!!                                                         !!
!!   See file rcsth.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!            

subroutine plane0_base(theta_0, curveA, ronde, ZR, ZI, Np)
implicit none
type(curve) :: curveA
real(wp) :: ronde
real(wp)    :: theta_0
integer, optional :: Np
real(wp), dimension(:) :: ZI, ZR
!! local variables
integer :: k, ks, N, i
!! mk : integration point 
real(wp), dimension(2) :: ak, bk, mk
type(segment) :: SK
!! Temporary variables
real(wp)    :: arg0
complex(wp) :: arg, Z
real(wp), allocatable, dimension(:,:) :: Gauss

if( (curveA%Ns .ne. size(ZR)) .or. &
    (curveA%Ns .ne. size(ZI)) )    &
    call MEXERRMSGTXT( 'Panic: Severe error in plane0')

N = 3 ! default number for Gauss points
if(present(Np)) N = Np

allocate(Gauss(3,N))
Gauss = setquad(N)

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   Z = 0
   do i=1,N
      mk = Gauss(1,i)*ak + Gauss(2,i)*bk
      arg0 = - (cos(theta_0)*mk(1) + sin(theta_0)*mk(2))
      arg  = ronde*cmplx(0._wp,arg0)
      Z    = Z - Gauss(3,i)*exp(arg)
   end do
   Z = Z*SK%l
   ZR(k) = real(Z); ZI(k) = imag(Z);
end do


end subroutine plane0_base


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function planeTHH0 returns the vector associated to the !!
!! linear form describing the power resulting from testing !!
!! the magnetic field relative to a plane wave with        !!
!! a complex amplitude equal to 1 for a TH polarization    !!
!! (field H is in the plan of propagation) by a test       !!
!! electric current J' being constant over each segment    !! 
!!                                                         !!
!! -\int_A  (-ikNu.n(x)) exp(-ik Nu.x) J'(x) dA(x) =       !!
!!                                      [J']^T [planeTHH0] !!
!!                                                         !!
!!  The normal n to curve A is fixed by the sense of the   !!
!!  numbering of the apices of the segments of A           !!
!!                                                         !!
!!     A : a curve meshed in segments                      !!
!!     J': generic function constant over each             !!
!!                  segment of A                           !!
!!     Nu = (cos(theta_0),sin(theta_0)) : (opposite)       !!
!!       direction of propagation of the incident wave     !!
!!                                                         !!
!!        [J']  : column-vector that components are        !!
!!     the values of J' on each segment                    !!
!!                                                         !!
!!     The postfix 0 refers to functions P_0 on segments   !!
!!                                                         !!
!!     INPUT: curveA, ronde                                !!
!!         - curveA,  mesh of A                            !!
!!         - ronde, wavenumber                             !!
!!         - theta_0, angle in radians giving the          !! 
!!           (opposite) direction of propagation           !!
!!           of the incident wave                          !!
!!                                                         !!
!!     OUTPUT: planeTHH0                                   !!
!!                                                         !!
!!   See file rcsth.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!            

subroutine dnpl0_base(theta_0, curveA, ronde, ZR, ZI, Np) 
implicit none
type(curve) :: curveA
real(wp) :: ronde
real(wp)    :: theta_0
integer, optional :: Np
real(wp), dimension(:) :: ZR, ZI 

!! local variables
complex(wp) :: Z
integer :: k, ks, N, i
!! mk : integration point 
real(wp), dimension(2) :: ak, bk, mk
type(segment) :: SK
!! Temporary variables
real(wp)    :: arg0, arg1
complex(wp) :: arg
real(wp), allocatable, dimension(:,:) :: Gauss

if( (curveA%Ns .ne. size(ZR)) .or. &
    (curveA%Ns .ne. size(ZI)) )    &
    call MEXERRMSGTXT( 'Panic: Severe error in plane0')

N = 3 ! default number for Gauss points
if(present(Np)) N = Np

allocate(Gauss(3,N))
Gauss = setquad(N)

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   Z = 0
   do i=1,N
      mk = Gauss(1,i)*ak + Gauss(2,i)*bk
      arg0 = - (cos(theta_0)*mk(1) + sin(theta_0)*mk(2))
      arg1 = cos(theta_0)*SK%n(1) + sin(theta_0)*SK%n(2)
      arg  = ronde*cmplx(0._wp,arg0)
      Z = Z + Gauss(3,i)*cmplx(0,arg1)*exp(arg)
   end do
   Z = ronde*SK%l*Z
   ZR(k) = real(Z)
   ZI(k) = imag(Z)
end do


end subroutine dnpl0_base

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function planeTEE1 returns the vector associated to the !!
!! linear form describing the power resulting from testing !!
!! the electric field relative to a plane wave with        !!
!! a complex amplitude for the magnetic field equal to 1   !!
!! for a TE polarization (field E is in the plan of        !!
!! propagation) by a test electric current J' being linear !!
!! over each segment. Be aware that the power is really    !!
!! obtained by multipling [planeTEE1] by the factor        !!
!! (1/ronde^2)                                             !!
!!                                                         !!
!! -\int_A  (-ikNu.n(x)) exp(-ik Nu.x) J'(x) dA(x) =       !!
!!                                      [J']^T [planeTEE1] !!
!!                                                         !!
!!  The normal n to curve A is fixed by the sense of the   !!
!!  numbering of the apices of the segments of A           !!
!!                                                         !!
!!     A : a curve meshed in segments                      !!
!!     J': generic continuous function linear over each    !!
!!                  segment of A                           !!
!!     Nu = (cos(theta_0),sin(theta_0)) : (opposite)       !!
!!       direction of propagation of the incident wave     !!
!!                                                         !!
!!        [J']  : column-vector that components are        !!
!!     the values of J' on the internal nodes              !!
!!                                                         !!
!!     The postfix 1 refers to functions linear on each    !!
!!     segment                                             !!
!!                                                         !!
!!     INPUT: curveA, ronde                                !!
!!         - curveA,  mesh of A                            !!
!!         - ronde, wavenumber                             !!
!!         - theta_0, angle in radians giving the          !!
!!           (opposite) direction of propagation           !!
!!           of the incident wave                          !!
!!                                                         !!
!!     OUTPUT: planeTEE1                                   !!
!!                                                         !!
!!   See file rcste.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine dnpl1_base(theta_0, curveA, ronde, ZR, ZI, Np) 
implicit none
type(curve) :: curveA 
real(wp) :: ronde
real(wp)    :: theta_0
integer, optional :: Np
integer     :: NA 
real(wp), dimension(:) :: ZR, ZI

!! local variables
complex(wp) :: Z
integer :: k, ks, N, i
!! mk : integration point 
real(wp), dimension(2) :: ak, bk, mk
type(segment) :: SK
!! Temporary variables
real(wp)    :: arg0, arg1
complex(wp) :: arg
real(wp), allocatable, dimension(:,:) :: Gauss

NA = size(curveA%Coord,2)

if( (NA .ne. size(ZR)) .or. &
    (NA .ne. size(ZI)) )    &
    call MEXERRMSGTXT( 'Panic: Severe error in dnpl1_base')

N = 3 ! default number for Gauss points
if(present(Np)) N = Np

allocate(Gauss(3,N))
Gauss = setquad(N)

!! Initialization 
ZR = 0._wp; ZI = 0._wp;

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   do i=1,N
      mk = Gauss(1,i)*ak + Gauss(2,i)*bk
      arg0 = - (cos(theta_0)*mk(1) + sin(theta_0)*mk(2))
      arg1 = cos(theta_0)*SK%n(1) + sin(theta_0)*SK%n(2)
      arg  = ronde*cmplx(0._wp,arg0)
      Z = SK%l*Gauss(3,i)*Gauss(1,i)*cmplx(0,arg1)*exp(arg)
      ZR(k)  = ZR(k) + real(Z)
      ZI(k)  = ZI(k) + imag(Z)
      Z = SK%l*Gauss(3,i)*Gauss(2,i)*cmplx(0,arg1)*exp(arg)
      ZR(ks) = ZR(ks) + real(Z)
      ZI(ks) = ZI(ks) + imag(Z)
   end do
end do

ZR = ronde*ZR
ZI = ronde*ZI

end subroutine dnpl1_base


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function planeTEH1 returns the vector associated to the !!
!! linear form related to the rhs of the MFIE for a TE     !!
!! polarization relatively to an incident plane wave with  !!
!! a magnetic field having 1 as complex amplitude          !!
!!                                                         !!
!! It is defined through the following relation            !!
!!                                                         !!
!! - \int_A  exp(-ik Nu.x) J'(x) dA(x) =                   !!
!!                                   [J']^T [planeTEH1]    !!
!!                                                         !!
!!  The normal n to curve A is fixed by the sense of the   !!
!!  numbering of the apices of the segments of A           !!
!!                                                         !!
!!     A : a curve meshed in segments                      !!
!!     J': generic continuous function linear over each    !!
!!                  segment of A                           !!
!!     Nu = (cos(theta_0),sin(theta_0)) : (opposite)       !!
!!       direction of propagation of the incident wave     !!
!!                                                         !!
!!        [J']  : column-vector that components are        !!
!!     the values of J' at the internal nodes              !!
!!                                                         !!
!!     The postfix 1 refers to functions linear on each    !!
!!     segment                                             !!
!!                                                         !!
!!     INPUT: curveA, ronde                                !!
!!         - curveA,  mesh of A                            !!
!!         - ronde, wavenumber                             !!
!!         - theta_0, angle in radians giving the          !!
!!           (opposite) direction of propagation           !!
!!           of the incident wave                          !!
!!                                                         !!
!!     OUTPUT: planeTEH1                                   !!
!!                                                         !!
!!   See file rcste.f90 for a demonstration of how to      !!
!!   use a call to this function (and some others) to      !!
!!   solve the reference problem related to the            !!
!!   computation of the RCS of a disc                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

subroutine plane1_base(theta_0, curveA, ronde, ZR, ZI, Np)
implicit none
type(curve) :: curveA
real(wp) :: ronde
real(wp)    :: theta_0
integer, optional :: Np
integer     :: NA
real(wp), dimension(:) :: ZR, ZI 

!! local variables
complex(wp) :: Z
integer :: k, ks, N, i
!! mk : integration point 
real(wp), dimension(2) :: ak, bk, mk
type(segment) :: SK
!! Temporary variables
real(wp)    :: arg0
complex(wp) :: arg
real(wp), allocatable, dimension(:,:) :: Gauss

NA = size(curveA%Coord,2)

if( (NA .ne. size(ZR)) .or. &
    (NA .ne. size(ZI)) )    &
    call MEXERRMSGTXT( 'Panic: Severe error in plane1')

N = 3 ! default number for Gauss points
if(present(Np)) N = Np

allocate(Gauss(3,N))
Gauss = setquad(N)


!! Initialization 
ZR = 0._wp; ZI = 0._wp;

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   do i=1,N
      mk = Gauss(1,i)*ak + Gauss(2,i)*bk
      arg0 = - (cos(theta_0)*mk(1) + sin(theta_0)*mk(2))
      arg  = ronde*cmplx(0._wp,arg0)
      Z = SK%l*Gauss(3,i)*Gauss(1,i)*exp(arg)
      ZR(k)  = ZR(k) - real(Z)
      ZI(k)  = ZI(k) - imag(Z)
      Z = SK%l*Gauss(3,i)*Gauss(2,i)*exp(arg)
      ZR(ks) = ZR(ks) - real(Z)
      ZI(ks) = ZI(ks) - imag(Z)
   end do
end do


end subroutine plane1_base
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function dnpoint1 returns the vector associated to the  !!
!! linear form                                             !!
!!                                                         !!
!! \int_A  (i/4)(-k(x-x_0)/|x-x_0|.n(x)) CH1(k|x-x_0|)     !!
!!                                     J'(x) dA(x) =       !!
!!                                      [J']^T [dnpoint1]  !!
!!                                                         !!
!!  The normal n to curve A is fixed by the sense of the   !!
!!  numbering of the apices of the segments of A           !!
!!                                                         !!
!!     A : a curve meshed in segments                      !!
!!     J': generic continuous function linear over each    !!
!!                  segment of A                           !!
!!     x_0 :  location of the point source                 !!
!!                                                         !!
!!        [J']  : column-vector that components are        !!
!!     the values of J' on the internal nodes              !!
!!                                                         !!
!!     The postfix 1 refers to functions linear on each    !!
!!     segment                                             !!
!!                                                         !!
!!     INPUT: curveA, k                                    !!
!!         - curveA,  mesh of A                            !!
!!         - k, wavenumber                                 !!
!!         - x_0, array with two real components           !!
!!                storing the location of the point-source !!
!!                                                         !!
!!     OUTPUT: dnpoint1                                    !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          
subroutine dnpoint1_base_r(x_0, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveB
real(wp)    :: kwave
real(wp)    :: x_0(2)
real(wp),  dimension(:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp):: ZE(2), ZT(2), PL(2,2) 
type(segment) :: SL
! local variables
real(wp)    :: dmin
logical :: Isnear
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns), G(Nr)


if( (size(curveB%Coord,2) .ne. size(ZR) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI ) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in dnpoint1')

ZR = 0.; ZI = 0; 

do l=1,curveB%Ns
    ls = l + 1
    if(ls .gt. size(curveB%Coord,2)) ls = 1
    al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
    SL = ptoseg(al,bl); PL = p1tond(SL)
    dmin = rlong(x_0,SL%m)
      if (dmin .le. epsilon(dmin)*SL%l) &
           & call MEXERRMSGTXT('Panic: source point too near to the mesh in dnpoint1') 
      Isnear = dmin .le. D*SL%l
      if (Isnear) then
         GS = GRX1_r(x_0,SL,kwave)
      else
         G = GX1_r(x_0,SL,kwave)
      end if
      do le =1,2
         if (Isnear) then
            ZT(le) = intdblx_near(x_0,SL,le-1, Ns)
            ZT(le) = ZT(le) + & 
                 (0._wp,0.25_wp)*kwave*intnumx(SL,le-1,GS,Ns)
         else
            ZT(le) = (0._wp,0.25_wp)*kwave*intnumx(SL,le-1,G,Nr)
         end if
      end do

      PL = transpose(PL)
      ZE(1) = PL(1,1)*ZT(1) + PL(1,2)*ZT(2)
      ZE(2) = PL(2,1)*ZT(1) + PL(2,2)*ZT(2)
 

      ZR(l )  = ZR(l ) + real(ZE(1))
      ZR(ls)  = ZR(ls) + real(ZE(2))
      ZI(l )  = ZI(l ) + imag(ZE(1))
      ZI(ls)  = ZI(ls) + imag(ZE(2))
end do

end subroutine dnpoint1_base_r


subroutine dnpoint0_base_r(x_0, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveB
real(wp)    :: kwave
real(wp)    :: x_0(2)
real(wp),  dimension(:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp):: ZE 
type(segment) :: SL
! local variables
real(wp)    :: dmin
logical :: Isnear
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns), G(Nr)


if( (curveB%Ns .ne. size(ZR) ) .or. & 
    (curveB%Ns .ne. size(ZI ) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in dnpoint1')

ZR = 0.; ZI = 0; 

do l=1,curveB%Ns
    ls = l + 1
    if(ls .gt. size(curveB%Coord,2)) ls = 1
    al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
    SL = ptoseg(al,bl);
    dmin = rlong(x_0,SL%m)
      if (dmin .le. epsilon(dmin)*SL%l) &
           & call MEXERRMSGTXT('Panic: source point too near to the mesh in dnpoint1') 
      Isnear = dmin .le. D*SL%l
      if (Isnear) then
         GS = GRX1_r(x_0,SL,kwave)
      else
         G = GX1_r(x_0,SL,kwave)
      end if
      
         if (Isnear) then
            ZE = intdblx_near(x_0,SL,0, Ns)
            ZE = ZE + & 
                 (0._wp,0.25_wp)*kwave*intnumx(SL,0,GS,Ns)
         else
            ZE = (0._wp,0.25_wp)*kwave*intnumx(SL,0,G,Nr)
         end if
      



      ZR(l )  = ZR(l ) + real(ZE)
      ZI(l )  = ZI(l ) + imag(ZE)
end do

end subroutine dnpoint0_base_r

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function point1 returns the vector associated to the    !!
!! linear form                                             !!
!!                                                         !!
!! \int_A -(i/4)CH0(k|x-x_0|)J'(x) dA(x) = [J']^T [point1] !!
!!                                                         !!
!!     A : a curve meshed in segments                      !!
!!     J': generic continuous function linear over each    !!
!!                  segment of A                           !!
!!     x_0 :  location of the point source                 !!
!!                                                         !!
!!        [J']  : column-vector that components are        !!
!!     the values of J' on the internal nodes              !!
!!                                                         !!
!!     The postfix 1 refers to functions linear on each    !!
!!     segment                                             !!
!!                                                         !!
!!     INPUT: curveA, k                                    !!
!!         - curveA,  mesh of A                            !!
!!         - k, wavenumber                                 !!
!!         - x_0, array with two real components           !!
!!                storing the location of the point-source !!
!!                                                         !!
!!     OUTPUT: point1                                      !!
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

          
subroutine point1_base_r(x_0, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveB
real(wp)    :: kwave
real(wp)    :: x_0(2)
real(wp),  dimension(:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls, ke, le 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp):: ZE(2), ZT(2), PL(2,2) 
type(segment) :: SL
! local variables
real(wp)    :: dmin
logical :: Isnear
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns), G(Nr)


if( (size(curveB%Coord,2) .ne. size(ZR) ) .or. & 
    (size(curveB%Coord,2) .ne. size(ZI ) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in point1')

ZR = 0.; ZI = 0; 

do l=1,curveB%Ns
    ls = l + 1
    if(ls .gt. size(curveB%Coord,2)) ls = 1
    al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
    SL = ptoseg(al,bl); PL = p1tond(SL)
    dmin = (rlong(x_0,SL%m) + rlong(x_0,SL%a) + &
                                        & rlong(x_0,SL%b))/3.
      if (dmin .le. epsilon(dmin)*SL%l) &
  & call MEXERRMSGTXT('Panic: source point too near to the mesh in dnpoint1') 
      Isnear = dmin .le. D*SL%l
      if (Isnear) then
         GS = GRX0_r(x_0,SL,kwave)
      else
         G = GX0_r(x_0,SL,kwave)
      end if
      do le =1,2
         if (Isnear) then
            ZT(le) = intsglx_near(x_0,SL,le-1, Ns)
            ZT(le) = ZT(le) + & 
                 (0._wp,0.25_wp)*intnumx(SL,le-1,GS,Ns)
         else
            ZT(le) = (0._wp,0.25_wp)*intnumx(SL,le-1,G,Nr)
         end if
      end do

      PL = transpose(PL)
      ZE(1) = PL(1,1)*ZT(1) + PL(1,2)*ZT(2)
      ZE(2) = PL(2,1)*ZT(1) + PL(2,2)*ZT(2)
 

      ZR(l )  = ZR(l ) - real(ZE(1))
      ZR(ls)  = ZR(ls) - real(ZE(2))
      ZI(l )  = ZI(l ) - imag(ZE(1))
      ZI(ls)  = ZI(ls) - imag(ZE(2))
end do

end subroutine point1_base_r


subroutine point0_base_r(x_0, curveB, kwave, ZR, ZI)
implicit none
type(curve) :: curveB
real(wp)    :: kwave
real(wp)    :: x_0(2)
real(wp),  dimension(:) :: ZR, ZI

!! local variables
integer :: k, l, ks, ls 
real(wp), dimension(2) :: ak, bk, al, bl
complex(wp):: ZE 
type(segment) :: SL
! local variables
real(wp)    :: dmin
logical :: Isnear
integer, parameter :: Nr = 2, Ns = 3 
complex(wp) :: GS(Ns), G(Nr)


if( (curveB%Ns .ne. size(ZR) ) .or. & 
    (curveB%Ns .ne. size(ZI) ) )    &
    call MEXERRMSGTXT( 'Panic: severe error in point0')

ZR = 0.; ZI = 0; 

do l=1,curveB%Ns
    ls = l + 1
    if(ls .gt. size(curveB%Coord,2)) ls = 1
    al = curveB%Coord(:,l); bl = curveB%Coord(:,ls)
    SL = ptoseg(al,bl); 
    dmin = (rlong(x_0,SL%m) + rlong(x_0,SL%a) + &
                                        & rlong(x_0,SL%b))/3.
      if (dmin .le. epsilon(dmin)*SL%l) &
  & call MEXERRMSGTXT('Panic: source point too near to the mesh in point0') 
      Isnear = dmin .le. D*SL%l
      if (Isnear) then
         GS = GRX0_r(x_0,SL,kwave)
      else
         G = GX0_r(x_0,SL,kwave)
      end if
      if (Isnear) then
            ZE = intsglx_near(x_0,SL,0, Ns)
            ZE = ZE + & 
                 (0._wp,0.25_wp)*intnumx(SL,0,GS,Ns)
      else
            ZE = (0._wp,0.25_wp)*intnumx(SL,0,G,Nr)
      end if

 
      ZR(l)  = - real(ZE)
      ZI(l)  = - imag(ZE)
end do

end subroutine point0_base_r


!************************************************************
!                                                           *
!                                                           *
!    P A R T  O F  T H E  C O D E  R E L A T I V E   T O    *
!           T H E  C O M P U T A T I O N  O F               *
!            F A R  F I E L D  P A T T E R N                *
!          O F  S I N G L E  A N D  D O U B l E             *
!                  P O T E N T I A L S                      *
!                      A N D  L I N E                       *
! E L E C T R I C  A N D  M A G N E T I C  C U R R E N T S  *
!                                                           *
!                                                           *
!                                                           *
!    The computation of radiation patterns, gain, RCS       *
!    can be obtained by simple expressions involving        *
!    these values                                           *
!************************************************************


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function farsngl0 returns the farfield pattern relative !!
!! to the field radiated by a single layer potential       !!
!! reative to a density \lambda on a curve A               !!
!!                                                         !!
!!      if  v(x) = \int_A G(x,y) \lambda(y) dA(y)          !!
!! its far field pattern  a(\theta)  is defined through    !!
!! the asymptotic behavior of v(x) at infinity             !!
!!  v(x) = (exp(i k r)/\sqrt(r)) a(\theta) + o(1/\sqrt(r)) !!
!!                                                         !!
!!           c = cos(\theta)     s = sin(\theta)           !!
!! a(\theta) = ((1+i)/(4\sqrt(k \pi)))                     !!
!!     \int_A exp(- i k (c y_1 + s y_2))\lambda(y) dA(y)   !!
!!                                                         !!
!!                Ending 0 refers to                       !!
!!       a function \lambda constant by element            !!
!!                                                         !!
!! INPUT: curveA, lambda, ronde, theta                     !!
!!     - curveA : mesh of curve A                          !!
!!     - lambda : values over each segment of the density  !!
!!     - ronde  : wavenumber k                             !!
!!     - theta  : angle in radians giving the direction    !!
!!                of the far field pattern                 !!
!!     - Np     : (optional) number of Gauss quadrature    !!
!!                points                                   !!
!!                                                         !!
!! theta can be either a scalar or a vector. In the latter !!
!! case, the far field pattern is computed for all the     !!
!! directions related to the values in theta and the       !!
!! results are returned in a vector of the same size.      !!
!! The interface which makes the right call is defined     !!
!! in the declaration block above                          !!  
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine farsngl0_base(curveA, JR, JI, ronde, theta, &
                                            &   ZR, ZI, Np)
implicit none
type(curve) :: curveA
real(wp) :: ronde
real(wp), dimension(:) :: theta
real(wp), dimension(:) :: ZR, ZI 
real(wp), dimension(:) :: JR, JI
integer, optional :: Np
!! local variables
real(wp),    dimension(size(theta)) :: c, s
complex(wp), dimension(size(theta)) :: arg
complex(wp) :: lambda
real(wp) :: ak(2), bk(2), mk(2)
type(segment) :: SK
integer  :: k, ks, N, i
complex(wp), dimension(size(theta)) :: Fk
real(wp), allocatable, dimension(:,:) :: Gauss

if ( (size(JR) .ne. curveA%Ns) .or. &
     (size(JI) .ne. curveA%Ns) )    &
     call MEXERRMSGTXT( 'Panic: Mesh and unknowns &
       & do not fit in function farsngl0_base') 

if( (size(Theta) .ne. size(ZR)) .or. &
    (size(Theta) .ne. size(ZI)) )    &
    call MEXERRMSGTXT( 'Panic: Severe error in farsngl0_base')


!! initialization
N = 3 ! default number for Gauss points
if(present(Np)) N = Np

allocate(Gauss(3,N))
Gauss = setquad(N)

ZR = 0.; ZI = 0.;
c = cos(theta); s = sin(theta)

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   Fk = 0._wp
   do i=1,N
      mk = Gauss(1,i)*ak + Gauss(2,i)*bk
      arg = -ronde*cmplx(0._wp,(mk(1)*c+mk(2)*s))
      lambda = cmplx(JR(k),JI(k))
      Fk = Fk + Gauss(3,i)*exp(arg)*lambda 
   end do
   ZR = ZR + SK%l*real(Fk*cmplx(1._wp,1._wp))
   ZI = ZI + SK%l*imag(Fk*cmplx(1._wp,1._wp))
end do

ZR = ZR/(4*sqrt(pi*ronde))
ZI = ZI/(4*sqrt(pi*ronde))

end subroutine farsngl0_base

subroutine farterj00_base(curveA, JR, JI, rhoR, rhoI, ronde, theta, &
                                     &   ZxR, ZxI, ZyR, ZyI, Np)
implicit none
type(curve) :: curveA
real(wp) :: ronde
real(wp), dimension(:)   :: theta
real(wp), dimension(:)   :: ZxR, ZxI, ZyR, ZyI 
real(wp), dimension(:)   :: JR, JI
real(wp), dimension(:)   :: rhoR, rhoI
integer, optional :: Np
!! local variables
real(wp),    dimension(size(theta)) :: c, s
complex(wp), dimension(size(theta)) :: arg
complex(wp) :: lambda, rho
real(wp) :: ak(2), bk(2), mk(2)
type(segment) :: SK
integer  :: k, ks, N, i
complex(wp), dimension(size(theta)) :: Fkx, Fky
real(wp), allocatable, dimension(:,:) :: Gauss

if ( (size(JR)   .ne. curveA%Ns) .or. &
     (size(JI)   .ne. curveA%Ns) .or. &
     (size(rhoR) .ne. curveA%Ns) .or. &
     (size(rhoI) .ne. curveA%Ns) )    &
     call MEXERRMSGTXT( &
 &'Panic: Mesh and unknowns do not fit in function farterj00_base') 

if( (size(Theta) .ne. size(ZxR)) .or. &
    (size(Theta) .ne. size(ZxI)) )    &
    call MEXERRMSGTXT( 'Panic: Severe error in farterj00_base')


!! initialization
N = 3 ! default number for Gauss points
if(present(Np)) N = Np

allocate(Gauss(3,N))
Gauss = setquad(N)

ZxR = 0.; ZxI = 0.; ZyR = 0.; ZyI = 0.;
c = cos(theta); s = sin(theta)

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. size(curveA%Coord,2)) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   Fkx = 0._wp; Fky = 0._wp;
   lambda = cmplx(JR(k),JI(k)); 
   rho = cmplx(rhoR(k),rhoI(k));
   do i=1,N
      mk = Gauss(1,i)*ak + Gauss(2,i)*bk
      arg = -ronde*cmplx(0._wp,(mk(1)*c+mk(2)*s))
      Fkx = Fkx + Gauss(3,i)*exp(arg)
      Fky = Fky + Gauss(3,i)*exp(arg)
   end do
   Fkx = Fkx*(lambda*SK%t(1) - rho*c)*SK%l
   Fky = Fky*(lambda*SK%t(2) - rho*s)*SK%l
   ZxR = ZxR + real(cmplx(0._wp,ronde)*Fkx*cmplx(1._wp,1._wp))
   ZyR = ZyR + real(cmplx(0._wp,ronde)*Fky*cmplx(1._wp,1._wp))
   ZxI = ZxI + imag(cmplx(0._wp,ronde)*Fkx*cmplx(1._wp,1._wp))
   ZyI = ZyI + imag(cmplx(0._wp,ronde)*Fky*cmplx(1._wp,1._wp))
end do

ZxR = ZxR/(4*sqrt(pi*ronde))
ZyR = ZyR/(4*sqrt(pi*ronde))
ZxI = ZxI/(4*sqrt(pi*ronde))
ZyI = ZyI/(4*sqrt(pi*ronde))

end subroutine farterj00_base


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function fardble1 returns the farfield pattern relative !!
!! to the field radiated by a double layer potential       !!
!! reative to a density J on a curve A                     !!
!!                                                         !!
!! if  v(x) = - \int_A \partial_{n_y) G(x,y) J(y) dA(y)    !!
!! its far field pattern  a(\theta)  is defined through    !!
!! the asymptotic behavior of v(x) at infinity             !!
!!  v(x) = (exp(i k r)/\sqrt(r)) a(\theta) + o(1/\sqrt(r)) !!
!!                                                         !!
!!           c = cos(\theta)     s = sin(\theta)           !!
!! a(\theta) = -((1+i)/(4\sqrt(k \pi)))                    !!
!! \int_A \partial_{n_y)exp(-ik(c y_1 + s y_2))J(y) dA(y)  !!
!!           = -((1+i)/(4\sqrt(k \pi)))                    !!
!! \int_A (-ik(cn_1+sn_2)exp(-ik(c y_1 + s y_2))J(y) dA(y) !!
!!                                                         !!
!!                Ending 1 refers to                       !!
!!    a continuous function J linear by element            !!
!!                                                         !!
!! INPUT: curveA, lambda, ronde, theta                     !!
!!     - curveA : mesh of curve A                          !!
!!     - lambda : values over each segment of the density  !!
!!     - ronde  : wavenumber k                             !!
!!     - theta  : angle in radians giving the direction    !!
!!                of the far field pattern                 !!
!!     - Np     : (optional) number of Gauss integration   !!
!!                points                                   !!
!!                                                         !!
!! theta can be either a scalar or a vector. In the latter !!
!! case, the far field pattern is computed for all the     !!
!! directions related to the values in theta and the       !!
!! results are returned in a vector of the same size.      !!
!! The interface which makes the right call is defined     !!
!! in the declaration block above                          !!  
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine fardble1_base(curveA,J_R,J_I,ronde,theta,ZR,ZI,Np)
implicit none
type(curve) :: curveA
real(wp) :: ronde
real(wp), dimension(:) :: theta, ZR, ZI
real(wp), dimension(:) :: J_R, J_I
integer, optional :: Np
!! local variables
real(wp),    dimension(size(theta)) :: c, s
complex(wp), dimension(size(theta)) :: arg
real(wp) :: ak(2), bk(2), mk(2)
type(segment) SK
complex(wp), dimension(size(theta)) :: F, Fk
integer  :: k, ks, N, i
real(wp), allocatable, dimension(:,:) :: Gauss
integer :: NA 

NA = size(curveA%Coord,2)

if ((size(J_R) .ne. NA) .or.  (size(J_I) .ne. NA) ) &
          &call MEXERRMSGTXT( 'Panic: Mesh and &
          &unknowns do not fit in function fardble1') 


If ( (size(Theta) .ne. size(ZR)) .or.  &
   & (size(Theta) .ne. size(ZR)) )     &
   call MEXERRMSGTXT( 'Panic: severe error in fardble1_base')


!! initialization

N = 3 ! default number for Gauss points
if(present(Np)) N = Np

allocate(Gauss(3,N))
Gauss = setquad(N)

ZR = 0.; ZI = 0.
c = cos(theta); s = sin(theta)

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. NA) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   Fk = 0._wp;
   do i=1,N
      mk = Gauss(1,i)*ak + Gauss(2,i)*bk
      arg = -ronde*cmplx(0._wp,(mk(1)*c+mk(2)*s))
      Fk = Fk + Gauss(3,i)* exp(arg)* & 
   & (Gauss(1,i)*cmplx(J_R(k),J_I(k)) + &
   &  Gauss(2,i)*cmplx(J_R(ks),J_I(ks)))
   end do
   F  = cmplx(1._wp,1._wp)*cmplx(0._wp,1._wp)*ronde* &
                  &  (SK%n(1)*c + SK%n(2)*s)*SK%l*Fk
   ZR = ZR + real(F)
   ZI = ZI + imag(F)
end do

F = F*cmplx(1._wp,1._wp)/(4*sqrt(pi*ronde))

ZR = ZR/(4*sqrt(pi*ronde))
ZI = ZI/(4*sqrt(pi*ronde));

end subroutine fardble1_base


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function farsngl1 returns the farfield pattern relative !!
!! to the field radiated by a double layer potential       !!
!! reative to a density J on a curve A                     !!
!!                                                         !!
!! if  v(x) = \int_A G(x,y) J(y) dA(y)                     !!
!! its far field pattern  a(\theta)  is defined through    !!
!! the asymptotic behavior of v(x) at infinity             !!
!!  v(x) = (exp(i k r)/\sqrt(r)) a(\theta) + o(1/\sqrt(r)) !!
!!                                                         !!
!!           c = cos(\theta)     s = sin(\theta)           !!
!! a(\theta) = ((1+i)/(4\sqrt(k \pi)))                     !!
!! \int_A exp(-ik(c y_1 + s y_2))J(y) dA(y)                !!
!!                                                         !!
!!                Ending 1 refers to                       !!
!!    a continuous function J linear by element            !!
!!                                                         !!
!! INPUT: curveA, lambda, ronde, theta                     !!
!!     - curveA : mesh of curve A                          !!
!!     - lambda : values over each segment of the density  !!
!!     - ronde  : wavenumber k                             !!
!!     - theta  : angle in radians giving the direction    !!
!!                of the far field pattern                 !!
!!     - Np     : (optional) number of Gauss integration   !!
!!                points                                   !!
!!                                                         !!
!! theta can be either a scalar or a vector. In the latter !!
!! case, the far field pattern is computed for all the     !!
!! directions related to the values in theta and the       !!
!! results are returned in a vector of the same size.      !!
!! The interface which makes the right call is defined     !!
!! in the declaration block above                          !!  
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine farsngl1_base(curveA,J_R,J_I,ronde,theta,ZR,ZI,Np)
implicit none
type(curve) :: curveA
real(wp) :: ronde
real(wp), dimension(:) :: theta, ZR, ZI
real(wp), dimension(:) :: J_R, J_I
integer, optional :: Np
!! local variables
real(wp),    dimension(size(theta)) :: c, s
complex(wp), dimension(size(theta)) :: arg
real(wp) :: ak(2), bk(2), mk(2)
type(segment) SK
complex(wp), dimension(size(theta)) :: F, Fk
integer  :: k, ks, N, i
real(wp), allocatable, dimension(:,:) :: Gauss
integer :: NA 

NA = size(curveA%Coord,2)

if ((size(J_R) .ne. NA) .or.  (size(J_I) .ne. NA) ) &
          &call MEXERRMSGTXT( 'Panic: Mesh and &
          &unknowns do not fit in function farsngl1') 


If ( (size(Theta) .ne. size(ZR)) .or.  &
   & (size(Theta) .ne. size(ZR)) )     &
   call MEXERRMSGTXT( 'Panic: severe error in farsngl1_base')


!! initialization

N = 3 ! default number for Gauss points
if(present(Np)) N = Np

allocate(Gauss(3,N))
Gauss = setquad(N)

ZR = 0.; ZI = 0.
c = cos(theta); s = sin(theta)

do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. NA) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   Fk = 0._wp;
   do i=1,N
      mk = Gauss(1,i)*ak + Gauss(2,i)*bk
      arg = -ronde*cmplx(0._wp,(mk(1)*c+mk(2)*s))
      Fk = Fk + Gauss(3,i)* exp(arg)* & 
   & (Gauss(1,i)*cmplx(J_R(k),J_I(k)) + &
   &  Gauss(2,i)*cmplx(J_R(ks),J_I(ks)))
   end do
   F  = cmplx(1._wp,1._wp)*SK%l*Fk
   ZR = ZR + real(F)
   ZI = ZI + imag(F)
end do

ZR = ZR/(4*sqrt(pi*ronde))
ZI = ZI/(4*sqrt(pi*ronde));

end subroutine farsngl1_base



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function fardble0 returns the farfield pattern relative !!
!! to the field radiated by a double layer potential       !!
!! reative to a density J on a curve A                     !!
!!                                                         !!
!! if  v(x) = - \int_A \partial_{n_y) G(x,y) J(y) dA(y)    !!
!! its far field pattern  a(\theta)  is defined through    !!
!! the asymptotic behavior of v(x) at infinity             !!
!!  v(x) = (exp(i k r)/\sqrt(r)) a(\theta) + o(1/\sqrt(r)) !!
!!                                                         !!
!!           c = cos(\theta)     s = sin(\theta)           !!
!! a(\theta) = -((1+i)/(4\sqrt(k \pi)))                    !!
!! \int_A \partial_{n_y)exp(-ik(c y_1 + s y_2))J(y) dA(y)  !!
!!           = -((1+i)/(4\sqrt(k \pi)))                    !!
!! \int_A (-ik(cn_1+sn_2)exp(-ik(c y_1 + s y_2))J(y) dA(y) !!
!!                                                         !!
!!                Ending 0 refers to                       !!
!!    a piecewise constant function J                      !!
!!                                                         !!
!! INPUT: curveA, lambda, ronde, theta                     !!
!!     - curveA : mesh of curve A                          !!
!!     - lambda : values over each segment of the density  !!
!!     - ronde  : wavenumber k                             !!
!!     - theta  : angle in radians giving the direction    !!
!!                of the far field pattern                 !!
!!     - Np     : (optional) number of Gauss integration   !!
!!                points                                   !!
!!                                                         !!
!! theta can be either a scalar or a vector. In the latter !!
!! case, the far field pattern is computed for all the     !!
!! directions related to the values in theta and the       !!
!! results are returned in a vector of the same size.      !!
!! The interface which makes the right call is defined     !!
!! in the declaration block above                          !!  
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine fardble0_base(curveA, JR, JI, ronde, theta, &
                                           &      ZR, ZI, Np)
implicit none
type(curve) :: curveA
real(wp) :: ronde
real(wp), dimension(:) :: theta
real(wp), dimension(:) :: ZR, ZI 
real(wp), dimension(:) :: JR, JI
integer, optional :: Np

!! local variables
real(wp),    dimension(size(theta)) :: c, s
complex(wp), dimension(size(theta)) :: arg
real(wp) :: ak(2), bk(2), mk(2)
type(segment) SK
complex(wp), dimension(size(theta)) :: Fk, F
integer  :: k, ks, N, i
real(wp), allocatable, dimension(:,:) :: Gauss
integer :: NA 

NA = curveA%Ns

if ( (size(JR) .ne. NA) .or. &
     (size(JI) .ne. NA) )    &
          &call MEXERRMSGTXT( 'Panic: Mesh and &
          &unknowns do not fit in function fardble0') 

!! initialization

N = 3 ! default number for Gauss points
if(present(Np)) N = Np

allocate(Gauss(3,N))
Gauss = setquad(N)

ZR = 0.; ZI = 0.;
c = cos(theta); s = sin(theta)



do k=1,curveA%Ns
   ks = k + 1
   if(ks .gt. NA) ks = 1
   ak = curveA%Coord(:,k); bk = curveA%Coord(:,ks)
   SK = ptoseg(ak,bk)
   Fk = 0._wp;
   do i=1,N
      mk = Gauss(1,i)*ak + Gauss(2,i)*bk
      arg = -ronde*cmplx(0._wp,(mk(1)*c+mk(2)*s))
      Fk = Fk + Gauss(3,i)* exp(arg)*cmplx(JR(k),JI(k))
   end do
   F  = cmplx(1._wp,1._wp)*cmplx(0._wp,1._wp)*ronde* &
                  &  (SK%n(1)*c + SK%n(2)*s)*SK%l*Fk
   ZR = ZR + real(F)
   ZI = ZI + imag(F)
end do

ZR = ZR/(4*sqrt(pi*ronde))
ZI = ZI/(4*sqrt(pi*ronde))

end subroutine fardble0_base


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!                                                         !!
!! Function farpoint returns the farfield pattern relative !!
!! to the field radiated by a point source                 !!
!!                      G(x,x_0)                           !!
!!                                                         !!
!! located at point x_0; function G(x,x_0) is the kernel   !!
!! giving the outgoing solution to the 2D Helmholtz        !!
!! equation                                                !!
!!                                                         !!
!! The far field pattern  a(\theta)  is defined through    !!
!! the asymptotic behavior of v(x) at infinity             !!
!!  v(x) = (exp(i k r)/\sqrt(r)) a(\theta) + o(1/\sqrt(r)) !!
!!                                                         !!
!!           c = cos(\theta)     s = sin(\theta)           !!
!! a(\theta) = ((1+i)/(4\sqrt(k \pi)))                     !!
!!        exp(- i k (c (x_0)_1 + s  (x_0)_2) )             !!
!!                                                         !!
!! (x_0)_1 and  (x_0)_2 : the two coordinates of x_0       !!
!!                                                         !!
!! INPUT: k, x_0, theta                                    !!
!!     - k      : wavenumber                               !!
!!     - x_0    : location of the source                   !!
!!     - theta  : angle in radians giving the direction    !!
!! toward which is computed the far field pattern          !!
!!                                                         !!
!! theta can be either a scalar or a vector. In the latter !!
!! case, the far field pattern is computed for all the     !!
!! directions related to the values in theta and the       !!
!! results are returned in a vector of the same size.      !!
!! The interface which makes the right call is defined     !!
!! in the declaration block above                          !!  
!!                                                         !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!$function farpoint_vector(k, x_0, theta) result(F)
!!$implicit none
!!$real(wp)    :: k, x_0(2)
!!$real(wp),    dimension(:) :: theta
!!$complex(wp), dimension(size(theta)) :: F
!!$!! local variables
!!$real(wp),    dimension(size(theta)) :: c, s
!!$complex(wp), dimension(size(theta)) :: arg
!!$
!!$
!!$c = cos(theta); s = sin(theta)
!!$arg = -cmplx(0._wp,k*(x_0(1)*c+x_0(2)*s))
!!$
!!$F = exp(arg)   
!!$
!!$F = F*cmplx(1._wp,1._wp)/(4*sqrt(pi*k))
!!$
!!$end function farpoint_vector
!!$


end module ie2mmex

























